REM #############################################################################################
REM ##### This script delete all devices and all *.o lib objects (the .lib are not deleted) #####
REM #############################################################################################

@echo off
cls

echo "####################### CLEANING LIBRARY #######################"
echo.
cd Libraries\
call .\WinCleanAllLibs.bat
echo.

cd ..

echo "####################### CLEANING DEVICES #######################"
echo.
cd Devices/ 
call .\WinCleanAllDevices.bat
echo.

cd ..

@echo on