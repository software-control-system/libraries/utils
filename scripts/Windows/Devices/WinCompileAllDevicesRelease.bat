REM ###############################################################################################################
REM ##### This script compile all devices in release using the SW_SUPPORT release libs of the Tango directory #####
REM ###############################################################################################################

@echo off

echo "###################### DEVICE BEAMLINE ENERGY PROXIMA #########################"
echo "Compiling Device BeamLineEnergy Proxima..."
cd BeamLineEnergyProxima\src\
nmake -f Makefile.vc 
echo.

echo "###################### DEVICE BEAMLINE ENERGY TEMPO #########################"
echo "Compiling Device BeamLineEnergy Tempo..."
cd ..\..\BeamLineEnergyProxima\src\
nmake -f Makefile.vc 
echo.

echo "###################### DEVICE BLADE GENERIC BENDER #########################"
echo "Compiling Device Blade Generic Bender..."
cd ..\..\BladeGenericBender\src\
nmake -f Makefile.vc 
echo.

echo "###################### DEVICE DATAFITTER #########################"
echo "Compiling Device DataFitter..."
cd ..\..\DataFitter\src\
nmake -f Makefile.vc 
echo.

echo "###################### DEVICE GENERIC MIRROR #########################"
echo "Compiling Device Generic Mirror..."
cd ..\..\GenericMirror\src\
nmake -f Makefile.vc
echo.

echo "###################### DEVICE MECHANICAL GENERIC BENDER #########################"
echo "Compiling Device Mechanical Generic Bender..."
cd ..\..\MechanicalGenericBender\src\
nmake -f Makefile.vc 
echo.

echo "############# DEVICE MONOCHROMATOR CASSIOPEE #################"
echo "Compiling Device MonochromatorCassiopee..."
cd ..\..\MonochromatorCassiopee\src\
nmake -f Makefile.vc 
echo.

echo "############# DEVICE MONOCHROMATOR CRISTAL #################"
echo "Compiling Device MonochromatorCristal..."
cd ..\..\MonochromatorCristal\src\
nmake -f Makefile.vc 
echo.

echo "############### DEVICE MONOCHROMATOR DESIR ###################"
echo "Compiling Device MonochromatorDesir..."
cd ..\..\MonochromatorDesir\src\
nmake -f Makefile.vc 
echo.

echo "############# DEVICE MONOCHROMATOR DIFFABS ###################"
echo "Compiling Device MonochromatorDiffabs..."
cd ..\..\MonochromatorDiffabs\src\
nmake -f Makefile.vc 
echo.

echo "############# DEVICE MONOCHROMATOR MARS ###################"
echo "Compiling Device MonochromatorMars..."
cd ..\..\MonochromatorMars\src\
nmake -f Makefile.vc 
echo.

echo "############# DEVICE MONOCHROMATOR ODE ###################"
echo "Compiling Device MonochromatorOde..."
cd ..\..\MonochromatorOde\src\
nmake -f Makefile.vc 
echo.

echo "############# DEVICE MONOCHROMATOR PROXIMA ###################"
echo "Compiling Device MonochromatorProxima..."
cd ..\..\MonochromatorProxima\src\
nmake -f Makefile.vc 
echo.

echo "############### DEVICE MONOCHROMATOR SAMBA ###################"
echo "Compiling Device MonochromatorSamba..."
cd ..\..\MonochromatorSamba\src\
nmake -f Makefile.vc 
echo.

echo "############### DEVICE MONOCHROMATOR SWING ###################"
echo "Compiling Device MonochromatorSwing..."
cd ..\..\MonochromatorSwing\src\
nmake -f Makefile.vc 
echo.

echo "############### DEVICE MONOCHROMATOR TEMPO ###################"
echo "Compiling Device MonochromatorTempo..."
cd ..\..\MonochromatorTempo\src\
nmake -f Makefile.vc 
echo.

echo "############### DEVICE ONDULATOR MASK ###################"
echo "Compiling Device OndulatorMask..."
cd ..\..\OndulatorMask\src\
nmake -f Makefile.vc 
echo.

echo "################### DEVICE TRAITPOINTPLAN ####################"
echo "Compiling Device TraitPointPlan..."
cd ..\..\TraitPointPlan\src\
nmake -f Makefile.vc 
echo.

echo "################### DEVICE TRIPLE GENERIC MIRROR ####################"
echo "Compiling Device TripleGenericMirror..."
cd ..\..\TripleGenericMirror\src\
nmake -f Makefile.vc 
echo.

cd ..\..

@echo on

