REM ##########################################
REM ##### This script delete all devices #####
REM ##########################################

@echo off

echo "##############################################################"
echo "Cleaning Device BeamLine Energy Proxima..."
cd BeamLineEnergyProxima\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device BeamLine Energy Tempo..."
cd ..\..\BeamLineEnergyTempo\src\
nmake -f Makefile.vc clean 
echo.

echo "##############################################################"
echo "Cleaning Device Blade Generic Bender..."
cd ..\..\BladeGenericBender\src\
nmake -f Makefile.vc clean 
echo.

echo "##############################################################"
echo "Cleaning Device DataFitter..."
cd ..\..\DataFitter\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device Generic Mirror..."
cd ..\..\GenericMirror\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device Mechanical Generic Bender..."
cd ..\..\MechanicalGenericBender\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorCassiopee..."
cd ..\..\MonochromatorCassiopee\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorCristal..."
cd ..\..\MonochromatorCristal\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorDesir..."
cd ..\..\MonochromatorDesir\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorDiffabs..."
cd ..\..\MonochromatorDiffabs\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorMars..."
cd ..\..\MonochromatorMars\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorOde..."
cd ..\..\MonochromatorOde\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorProxima..."
cd ..\..\MonochromatorProxima\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorSamba..."
cd ..\..\MonochromatorSamba\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorSwing..."
cd ..\..\MonochromatorSwing\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device MonochromatorTempo..."
cd ..\..\MonochromatorTempo\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device OndulatorMask..."
cd ..\..\OndulatorMask\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device TraitPointPlan..."
cd ..\..\TraitPointPlan\src\
nmake -f Makefile.vc clean
echo.

echo "##############################################################"
echo "Cleaning Device Triple Generic Mirror..."
cd ..\..\TripleGenericMirror\src\
nmake -f Makefile.vc clean
echo.

cd ..\..

echo "##############################################################"
echo "Removing *.*.bak in src..."
del BeamLineEnergyProxima\src\*.*.bak
del BeamLineEnergyTempo\src\*.*.bak
del BladeGenericBender\src\*.*.bak
del DataFitter\src\*.*.bak
del GenericMirror\src\*.*.bak
del MechanicalGenericBender\src\*.*.bak
del MonochromatorCassiopee\src\*.*.bak
del MonochromatorCristal\src\*.*.bak
del MonochromatorDesir\src\*.*.bak
del MonochromatorDiffabs\src\*.*.bak
del MonochromatorMars\src\*.*.bak
del MonochromatorOde\src\*.*.bak
del MonochromatorProxima\src\*.*.bak
del MonochromatorSamba\src\*.*.bak
del MonochromatorSwing\src\*.*.bak
del MonochromatorTempo\src\*.*.bak
del OndulatorMask\src\*.*.bak
del TraitPointPlan\src\*.*.bak
del TripleGenericMirror\src\*.*.bak
echo "##############################################################"

@echo on