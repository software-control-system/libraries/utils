REM #######################################################################################
REM ##### This script delete all .o files for all libraries (the .lib are not deleted #####
REM #######################################################################################

REM set SW_SUPPORT= ../..

@echo off
cls

echo "####################### ADTB LIBRARY #######################"
echo "Cleaning Library ADTB..."
cd ADTB\src
nmake -f Makefile.vc clean
echo.

echo "#################### BEAMLINE ENERGY LIBRARY ######################"
echo "Cleaning Library BeamLine Energy..."
cd ..\..\BeamLineEnergy\src
nmake -f Makefile.vc clean
echo.

echo "#################### DATAFITTER LIBRARY ######################"
echo "Cleaning Library DataFitter..."
cd ..\..\DataFitterLib\src
nmake -f Makefile.vc clean
echo.

echo "#################### EXCEPTIONS LIBRARY ######################"
echo "Cleaning Library Exceptions..."
cd ..\..\Exceptions\src
nmake -f Makefile.vc clean
echo.

echo "####################### GENERIC BENDER LIBRARY #######################"
echo "Cleaning Library Generic Bender..."
cd ..\..\GenericBender\src\
nmake -f Makefile.vc clean
echo.

echo "############## GRATING MONOCHROMATOR LIBRARY #################"
echo "Cleaning Library GratingMonochromator..."
cd ..\..\GratingMonochromator\src
nmake -f Makefile.vc clean
DEL /F /Q c:\Temp\%USERNAME%\libGratingMonochromator
echo.

echo "################### INTERPOLATOR LIBRARY #####################"
echo "Cleaning Library Interpolator..."
cd ..\..\Interpolator\src
nmake -f Makefile.vc clean
echo.

echo "################### MIRROR LIBRARY #####################"
echo "Cleaning Library Mirror..."
cd ..\..\Mirror\src
nmake -f Makefile.vc clean
echo.

echo "################## MONOCHROMATOR LIBRARY #####################"
echo "Cleaning Library Monochromator..."
cd ..\..\Monochromator\src
nmake -f Makefile.vc clean
DEL /F /Q c:\Temp\%USERNAME%\libMonochromator
echo.

echo "###################### UTILS LIBRARY #########################"
echo "Cleaning Library Utils..."
cd ..\..\Utils\src
nmake -f Makefile.vc clean
echo.

cd ..\..
echo "######################## INCLUDE ##############################"
echo "Removing *.*.bak in include"
del ADTB\include\*.*.bak
del BeamLineEnergy\include\*.*.bak
del DataFitter\include\*.*.bak
del Exceptions\include\*.*.bak
del GenericBender\include\*.*.bak
del GratingMonochromator\include\*.*.bak
del Interpolator\include\*.*.bak
del Mirror\include\*.*.bak
del Monochromator\include\*.*.bak
del Utils\include\*.*.bak
echo.

echo "########################### SRC ###############################"
echo "Removing *.*.bak in src"
del ADTB\src\*.*.bak
del BeamLineEnergy\src\*.*.bak
del DataFitter\src\*.*.bak
del Exceptions\src\*.*.bak
del GenericBender\src\*.*.bak
del GratingMonochromator\src\*.*.bak
del Interpolator\src\*.*.bak
del Mirror\src\*.*.bak
del Monochromator\src\*.*.bak
del Utils\src\*.*.bak
echo.

@echo on