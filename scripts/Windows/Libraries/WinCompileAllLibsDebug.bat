REM #################################################
REM ##### This script compile all libs in DEBUG #####
REM #################################################

REM set SW_SUPPORT=../..

@echo off

echo "####################### ADTB LIBRARY #######################"
echo "Compiling Library ADTB..."
cd ADTB\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "#################### BEAMLINE ENERGY LIBRARY ######################"
echo "Compiling Library BeamLine Energy..."
cd ..\..\BeamLineEnergy\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "#################### DATAFITTER LIBRARY ######################"
echo "Compiling Library DataFitter..."
cd ..\..\DataFitterLib\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "#################### EXCEPTIONS LIBRARY ######################"
echo "Compiling Library Exceptions..."
cd ..\..\Exceptions\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "####################### GENERIC BENDER LIBRARY #######################"
echo "Compiling Library Generic Bender..."
cd ..\..\GenericBender\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "############## GRATING MONOCHROMATOR LIBRARY #################"
echo "Compiling Library GratingMonochromator..."
cd ..\..\GratingMonochromator\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "######################## GSL LIBRARY #########################"
echo "Compiling Library GSL..."
cd ..\..\GSL\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "################### INTERPOLATOR LIBRARY #####################"
echo "Compiling Library Interpolator..."
cd ..\..\Interpolator\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "################## MIRROR LIBRARY #####################"
echo "Compiling Library Mirror..."
cd ..\..\Mirror\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "################## MONOCHROMATOR LIBRARY #####################"
echo "Compiling Library Monochromator..."
cd ..\..\Monochromator\src\
nmake -f Makefile.vc DEBUG=
echo.

echo "###################### UTILS LIBRARY #########################"
echo "Compiling Library Utils..."
cd ..\..\Utils\src\
nmake -f Makefile.vc DEBUG=
echo.

cd ..\..

@echo on
