REM ################################################################
REM ##### This script compile all libs in DEBUG and in RELEASE #####
REM ################################################################

REM set SW_SUPPORT=../..

@echo off
cls

echo "############################## COMPILE LIBRARIES IN DEBUG ##############################"
echo.
rem call WinCleanAllLibs.bat
call WinCompileAllLibsDebug.bat
echo.

echo "############################## COMPILE LIBRARIES IN RELEASE ##############################"
echo.
rem call WinCleanAllLibs.bat
call WinCompileAllLibsRelease.bat
echo.

@echo on

