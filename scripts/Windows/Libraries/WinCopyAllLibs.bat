REM ##########################################################################################################
REM ##### This script copy all libs and all include file in the SW_SUPPORT directory for a TANGO release #####
REM ##########################################################################################################

@echo off
cls


REM Copy dans la distrib
copy BeamLineEnergy\lib\libBeamLineEnergy*.lib 			"%SOLEIL_ROOT%\sw-support\BeamLineEnergy\lib\"
copy DataFitterLib\lib\libDataFitter*.lib 				"%SOLEIL_ROOT%\sw-support\DataFitterLib\lib\"
copy Exceptions\lib\libExceptions*.lib 				"%SOLEIL_ROOT%\sw-support\Exceptions\lib\"
copy GenericBender\lib\libGenericBender*.lib 			"%SOLEIL_ROOT%\sw-support\GenericBender\lib\"
copy GratingMonochromator\lib\libGratingMonochromator*.lib 	"%SOLEIL_ROOT%\sw-support\GratingMonochromator\lib\"
copy Interpolator\lib\libInterpolator*.lib 			"%SOLEIL_ROOT%\sw-support\Interpolator\lib\"
copy Mirror\lib\libMirror*.lib 					"%SOLEIL_ROOT%\sw-support\Mirror\lib\"
copy Monochromator\lib\libMonochromator*.lib 			"%SOLEIL_ROOT%\sw-support\Monochromator\lib\"
copy Utils\lib\libUtils*.lib 					"%SOLEIL_ROOT%\sw-support\Utils\lib\"

copy BeamLineEnergy\include\*.h					"%SOLEIL_ROOT%\sw-support\BeamLineEnergy\include\"
copy DataFitterLib\include\*.h					"%SOLEIL_ROOT%\sw-support\DataFitterLib\include\"
copy Exceptions\include\*.h 					"%SOLEIL_ROOT%\sw-support\Exceptions\include\"
copy GenericBender\include\*.h 					"%SOLEIL_ROOT%\sw-support\GenericBender\include\"
copy GratingMonochromator\include\*.h 				"%SOLEIL_ROOT%\sw-support\GratingMonochromator\include\"
copy Interpolator\include\*.h 					"%SOLEIL_ROOT%\sw-support\Interpolator\include\"
copy Mirror\include\*.h 					"%SOLEIL_ROOT%\sw-support\Mirror\include\"
copy Monochromator\include\*.h 					"%SOLEIL_ROOT%\sw-support\Monochromator\include\"
copy Utils\include\*.h 						"%SOLEIL_ROOT%\sw-support\Utils\include\"

copy BeamLineEnergy\doc\doxygen\html\*					"%SOLEIL_ROOT%\sw-support\BeamLineEnergy\doc\Doxygen\html"
copy DataFitterLib\doc\doxygen\html\*			"%SOLEIL_ROOT%\sw-support\DatafitterLib\doc\Doxygen\html\"
copy Exceptions\doc\Doxygen\html\* 					"%SOLEIL_ROOT%\sw-support\Exceptions\doc\Doxygen\html\"
copy GenericBender\doc\doxygen\html\* 					"%SOLEIL_ROOT%\sw-support\GenericBender\doc\Doxygen\html\"
copy GratingMonochromator\doc\doxygen\html\* 				"%SOLEIL_ROOT%\sw-support\GratingMonochromator\doc\Doxygen\html\"
copy Interpolator\doc\doxygen\html\*				"%SOLEIL_ROOT%\sw-support\Interpolator\doc\Doxygen\html\"
copy Mirror\doc\doxygen\html\* 					"%SOLEIL_ROOT%\sw-support\Mirror\doc\Doxygen\html\"
copy Monochromator\doc\doxygen\html\* 					"%SOLEIL_ROOT%\sw-support\Monochromator\doc\Doxygen\html\"
copy Utils\doc\Doxygen\html\* 						"%SOLEIL_ROOT%\sw-support\Utils\doc\Doxygen\html\"

@echo on