REM ##################################################################################################################################
REM ##### This script compile all libs and all devices in debug mode using the SW_SUPPORT env variable for the path to libraries #####
REM ##################################################################################################################################

@echo off
cls

echo "############################## COMPILE LIBRARIES ##############################"
echo.
cd Libraries
call WinCompileAllLibsDebug.bat
echo.

cd ..

echo "############################## COMPILE DEVICES ##############################"
echo.
cd Devices
call WinCompileAllDevicesDevDebug.bat
echo.

cd ..

@echo on
