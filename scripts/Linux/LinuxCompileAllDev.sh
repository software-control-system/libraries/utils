#!/bin/bash


echo "############################## COMPILE LIBRARIES ##############################"
echo ""
cd Libraries
LinuxCompileAllLibs.sh
echo ""

echo "############################### COMPILE DEVICES ###############################"
echo ""
cd ../Devices
LinuxCompileAllDevicesDev.sh
echo ""

cd ..