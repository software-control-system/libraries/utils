#!/bin/bash

echo ""
echo "####################### READING COMMAND CHOICE #######################"
if [ "$1" = "-co" ] || [ "$1" = "-checkout" ]
then	
	export COMMAND_CHOICE=co
	echo "Checkout command choosen..."
elif [ "$1" = "-update" ] 
then	
	export COMMAND_CHOICE=update
	echo "Update command choosen..."
else 
	echo "USAGE : cvscommand [-co|-checkout|-update] [-SOLEIL|-SOURCEFORGE] ModuleList.txt"
	exit 0
fi

echo ""
echo "####################### READING CVS REPOSITORY CHOICE #######################"
if [ "$2" = "-SOLEIL" ]
then	
	export CVS_REPOSITORY_CHOICE=SOLEIL
	echo "SOLEIL CVS REPOSITORY choosen..."
elif [ "$2" = "-SOURCEFORGE" ] 
then	
	export CVS_REPOSITORY_CHOICE=SOURCEFORGE
	echo "SOURCEFORGE CVS REPOSITORY choosen..."
else 
	echo "USAGE : cvscommand [-co|-checkout|-update] [-SOLEIL|-SOURCEFORGE] ModuleList.txt"
	exit 0
fi

echo ""
echo "####################### SETTING ENVIRONMENT VARIABLES #######################"

case $CVS_REPOSITORY_CHOICE in
	SOLEIL)
		export CVSROOT=:ext:postollec@ganymede:/usr/local/CVS
		export CVS_RSH=/usr/bin/ssh
		#export CVSROOT=:ext:postollec@passerelle:/usr/local/CVS
		#export CVS_RSH=/usr/Local/scripts/ssh2000
		;;
	SOURCEFORGE)
		export CVSROOT=:ext:stephpsoleil@tango-ds.cvs.sf.net:/cvsroot/tango-ds
		export CVS_RSH=/usr/Local/scripts/ssh2001
		;;
esac

echo "CVSROOT -->" $CVSROOT
echo "CVS_RSH -->" $CVS_RSH

echo ""
echo "####################### READING MODULE(S) #######################"

LIST_MODULES=$(echo $(cat "$3"))

set $LIST_MODULES
echo "Number of Modules to $COMMAND_CHOICE = $#"

for i in $LIST_MODULES
do
	echo $i
done

echo ""
echo "####################### $COMMAND_CHOICE MODULES #######################"

cvs $COMMAND_CHOICE $LIST_MODULES
