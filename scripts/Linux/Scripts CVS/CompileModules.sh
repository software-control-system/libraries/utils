#!/bin/bash

#You must be in the Optics/Repository directory!!!
REPOSITORY_PATH=$(echo $(pwd))

echo $REPOSITORY_PATH

echo ""
echo "####################### READING COMMAND CHOICE #######################"
if [ "$1" = "-debug" ] 
then	
	export COMMAND_CHOICE=Debug
elif [ "$1" = "-release" ] 
then	
	export COMMAND_CHOICE=Release
else 
	echo "USAGE : CompileModules [-debug|-release] [-LIB|-SOLEIL] [-library|-devices] ModulesList.txt"
	exit 0
fi

echo "$COMMAND_CHOICE compilation choosen..."

echo ""
echo "####################### READING LIBRARY USED CHOICE #######################"
if [ "$2" = "-LIB" ]
then	
	export SW_SUPPORT=$REPOSITORY_PATH/Libraries
	echo "CUSTOM PATH LIBRARY USED choosen..."
elif [ "$2" = "-SOLEIL" ] 
then	
	export SW_SUPPORT=$SOLEIL_ROOT/sw-support
	echo "SOLEIL PATH LIBRARIES USED choosen..."
else 
	echo "USAGE : CompileModules [-debug|-release] [-LIB|-SOLEIL] [-library|-device] ModulesList.txt"
	exit 0
fi

echo "SW_SUPPORT -->" $SW_SUPPORT

echo ""
echo "####################### READING MODULE TYPE TO COMPILE CHOICE #######################"
if [ "$3" = "-library" ]
then	
	export MODULE_PATH_CHOICE=$REPOSITORY_PATH/Libraries
	export MODULE_TYPE=LIBRARY
	echo "LIBRARY compilation choosen..."
elif [ "$3" = "-device" ] 
then	
	export MODULE_PATH_CHOICE=$REPOSITORY_PATH/Devices
	export MODULE_TYPE=DEVICE
	echo "DEVICE compilation choosen..."
else 
	echo "USAGE : CompileModules [-debug|-release] [-LIB|-SOLEIL] [-library|-device] ModulesList.txt"
	exit 0
fi

echo "MODULE_TYPE --> $MODULE_TYPE"
echo "MODULE_PATH_CHOICE --> $MODULE_PATH_CHOICE"

echo ""
echo "####################### SETTING MAKEFILE NAME TO COMPILE #######################"
if [ "$MODULE_TYPE" = "LIBRARY" ]
then	
	export MAKEFILE=Makefile.linux
elif [ "$MODULE_TYPE" = "DEVICE" ] 
then	
	if [ "$2" = "-LIB" ]
	then
		export MAKEFILE=MakefileDev.linux
	elif [ "$2" = "-SOLEIL" ]
	then
		export MAKEFILE=Makefile.linux	
	else
		echo "USAGE : CompileModules [-debug|-release] [-LIB|-SOLEIL] [-library|-device] ModulesList.txt"
		exit 0
	fi
else 
	echo "USAGE : CompileModules [-debug|-release] [-LIB|-SOLEIL] [-library|-device] ModulesList.txt"
	exit 0
fi

echo "MAKEFILE --> $MAKEFILE"

echo ""
echo "####################### READING MODULE(S) #######################"

LIST_MODULES=$(echo $(cat "$4"))

set $LIST_MODULES
echo "Number of $MODULE_TYPE to compile in $COMMAND_CHOICE mode = $#"

for i in $LIST_MODULES
do
	echo $i
done

echo ""
echo "####################### COMPILING $MODULE_TYPE(S) #######################"

for i in $LIST_MODULES
do
	echo ""
	echo "####################### $i $MODULE_TYPE #######################"
	echo "Compiling $MODULE_TYPE $i..."
	cd $MODULE_PATH_CHOICE/$i/src
	make -f ./$MAKEFILE 
	cd ../../..
	echo ""
done



