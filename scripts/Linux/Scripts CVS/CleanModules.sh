#!/bin/bash

#You must be in the Optics/Repository directory!!!
REPOSITORY_PATH=$(echo $(pwd))

echo $REPOSITORY_PATH

echo ""
echo "####################### READING MODULE TYPE TO COMPILE CHOICE #######################"
if [ "$1" = "-library" ]
then	
	export MODULE_PATH_CHOICE=$REPOSITORY_PATH/Libraries
	export MODULE_TYPE=LIBRARY
	echo "LIBRARY compilation choosen..."
elif [ "$1" = "-device" ] 
then	
	export MODULE_PATH_CHOICE=$REPOSITORY_PATH/Devices
	export MODULE_TYPE=DEVICE
	echo "DEVICE compilation choosen..."
else 
	echo "USAGE : CleanModules [-library|-device] ModulesList.txt"
	exit 0
fi

echo "MODULE_TYPE --> $MODULE_TYPE"
echo "MODULE_PATH_CHOICE --> $MODULE_PATH_CHOICE"

export MAKEFILE="Makefile.linux clean"
echo "MAKEFILE --> $MAKEFILE"

echo ""
echo "####################### READING MODULE(S) #######################"

LIST_MODULES=$(echo $(cat "$2"))

set $LIST_MODULES
echo "Number of $MODULE_TYPE to compile in $COMMAND_CHOICE mode = $#"

for i in $LIST_MODULES
do
	echo $i
done

echo ""
echo "####################### CLEANING $MODULE_TYPE(S) #######################"

for i in $LIST_MODULES
do
	echo ""
	echo "####################### $i $MODULE_TYPE #######################"
	echo "Cleaning $MODULE_TYPE $i..."
	cd $MODULE_PATH_CHOICE/$i/src
	make -f ./$MAKEFILE
	cd ../../..
	echo ""
done



