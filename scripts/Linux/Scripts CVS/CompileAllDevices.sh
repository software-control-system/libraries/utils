#You must be in the Optics/Repository directory!!!
REPOSITORY_PATH=$(echo $(pwd))

echo $REPOSITORY_PATH

SOLEIL_DEVICE_MODULES_FILE=ListDevicesSoleil.txt
SOURCEFORGE_DEVICE_MODULES_FILE=ListDevicesSourceforge.txt

$REPOSITORY_PATH/CompileModules.sh -debug -LIB -device $SOLEIL_DEVICE_MODULES_FILE
$REPOSITORY_PATH/CompileModules.sh -debug -LIB -device $SOURCEFORGE_DEVICE_MODULES_FILE
