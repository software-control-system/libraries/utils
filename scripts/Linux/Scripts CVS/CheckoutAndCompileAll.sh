#!/bin/bash

#You must be in the Optics/Repository directory!!!
REPOSITORY_PATH=$(echo $(pwd))
echo $REPOSITORY_PATH

LIBRARIES_PATH=$REPOSITORY_PATH/Libraries
DEVICES_PATH=$REPOSITORY_PATH/Devices

$REPOSITORY_PATH/CheckoutAll.sh
$REPOSITORY_PATH/CleanAll.sh
$REPOSITORY_PATH/CompileAll.sh


#Get the scripts in Utils
#cp $LIBRARIES_PATH/Utils/scripts/Linux/Libraries/*.sh 	$LIBRARIES_PATH
#cp $LIBRARIES_PATH/Utils/scripts/Linux/Devices/*.sh 	$DEVICES_PATH

#chmod a+x $LIBRARIES_PATH/*.sh
#chmod a+x $LIBRARIES_PATH/*.sh

#$LIBRARIES_PATH/LinuxCleanAllLibs.sh
#$LIBRARIES_PATH/LinuxCompileAllLibs.sh

#$DEVICES_PATH/LinuxCleanAllDevices.sh
#$DEVICES_PATH/LinuxCompileAllDevicesDev.sh
