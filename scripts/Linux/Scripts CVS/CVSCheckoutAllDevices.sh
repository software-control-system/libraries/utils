#!/bin/bash

#You must be in the Optics/Repository directory!!!
REPOSITORY_PATH=$(echo $(pwd))

SOLEIL_DEVICE_MODULES_FILE=ListDevicesSoleil.txt
SOURCEFORGE_DEVICE_MODULES_FILE=ListDevicesSourceforge.txt

echo $REPOSITORY_PATH

echo ""
echo "####################### CREATING DEVICES DIRECTORY #######################"
echo "Creating Devices directory..."
mkdir -p Devices
cd Devices

$REPOSITORY_PATH/CVSCommand.sh -co -SOLEIL 		$REPOSITORY_PATH/$SOLEIL_DEVICE_MODULES_FILE
$REPOSITORY_PATH/CVSCommand.sh -co -SOURCEFORGE 	$REPOSITORY_PATH/$SOURCEFORGE_DEVICE_MODULES_FILE


