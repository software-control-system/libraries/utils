#!/bin/bash

#Must get the scripts on this URL before
#wget http://controle/outils/postollec/Deployment.zip 
#unzip Deployment.zip
#and unzip in ~/Deployment

#To be on ~
cd ~ 

echo ""
echo "####################### CREATING REPOSITORY DIRECTORY #######################"
mkdir Optics
cd Optics
mkdir Repository
cd Repository

#You must be in the Optics/Repository directory!!!
REPOSITORY_PATH=$(echo $(pwd))
echo $REPOSITORY_PATH

DEPLOYMENT_PATH=~/Deployment

cp $DEPLOYMENT_PATH/*.sh .

echo ""
echo "####################### READING COMMAND CHOICE #######################"

case $1 in
Cassiopee)
	cp $DEPLOYMENT_PATH/Cassiopee/*.txt .	
;;
Cristal)
	cp $DEPLOYMENT_PATH/Cristal/*.txt .
;;
Desir)
	cp $DEPLOYMENT_PATH/Desir/*.txt .
;;
Diffabs)
	cp $DEPLOYMENT_PATH/Diffabs/*.txt .
;;
Mars)
	cp $DEPLOYMENT_PATH/Mars/*.txt .
;;
Ode)
	cp $DEPLOYMENT_PATH/Ode/*.txt .
;;
Proxima)
	cp $DEPLOYMENT_PATH/Proxima/*.txt .
;;
Samba)
	cp $DEPLOYMENT_PATH/Samba/*.txt .
;;
Swing)
	cp $DEPLOYMENT_PATH/Swing/*.txt .
;;
Tempo)
	cp $DEPLOYMENT_PATH/Tempo/*.txt .
;;
esac

echo ""
echo "####################### LAUNCHING CHECKOUT AND COMPILATION #######################"	
$REPOSITORY_PATH/CheckoutAndCompileAll.sh	




