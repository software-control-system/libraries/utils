#!/bin/bash

echo "####################### BEAM LINE ENERGY LIBRARY #######################"
echo "Cleaning Library Beam Line Energy..."
cd BeamLineEnergy/src
make -f ./Makefile.linux clean
echo ""

echo "#################### DATAFITTERLIB LIBRARY ######################"
echo "Cleaning Library DataFitterLib..."
cd ../../DataFitterLIb/src 
make -f ./Makefile.linux clean
echo ""

echo "#################### EXCEPTIONS LIBRARY ######################"
echo "Cleaning Library Exceptions..."
cd ../../Exceptions/src 
make -f Makefile.linux clean
echo ""

echo "####################### GENERIC BENDER LIBRARY #######################"
echo "Cleaning Library Generic Bender..."
cd ../../GenericBender/src
make -f ./Makefile.linux clean 
echo ""

echo "############## GRATING MONOCHROMATOR LIBRARY #################"
echo "Cleaning Library GratingMonochromator..."
cd ../../GratingMonochromator/src 
make -f Makefile.linux clean
echo ""

echo "################### INTERPOLATOR LIBRARY #####################"
echo "Cleaning Library Interpolator..."
cd ../../Interpolator/src 
make -f Makefile.linux clean
echo ""

echo "################## MIRROR LIBRARY #####################"
echo "Cleaning Library Mirror..."
cd ../../Mirror/src 
make -f Makefile.linux clean
echo ""

echo "################## MONOCHROMATOR LIBRARY #####################"
echo "Cleaning Library Monochromator..."
cd ../../Monochromator/src 
make -f Makefile.linux clean
echo ""

echo "###################### UTILS LIBRARY #########################"
echo "Cleaning Library Utils..."
cd ../../Utils/src 
make -f Makefile.linux clean
echo ""

cd ../.. 

echo "######################## INCLUDE ##############################"
echo "Removing *.*~ in include"
rm -f BeamLineEnergy/include/*.*~
rm -f DataFitterLib/include/*.*~
rm -f Exceptions/include/*.*~
rm -f GenericBender/include/*.*~
rm -f GratingMonochromator/include/*.*~
rm -f Interpolator/include/*.*~
rm -f Mirror/include/*.*~
rm -f Monochromator/include/*.*~
rm -f Utils/include/*.*~
echo ""

echo "########################### SRC ###############################"
echo "Removing *.*~ in src"
rm -f BeamLineEnergy/src/*.*~
rm -f DataFitterLib/src/*.*~
rm -f Exceptions/src/*.*~
rm -f GenericBender/src/*.*~
rm -f GratingMonochromator/src/*.*~
rm -f Interpolator/src/*.*~
rm -f Mirror/src/*.*~
rm -f Monochromator/src/*.*~
rm -f Utils/src/*.*~
echo ""
