#!/bin/bash

echo "####################### BEAM LINE ENERGY LIBRARY #######################"
echo "Compiling Library Beam Line Energy..."
cd BeamLineEnergy/src
make -f ./Makefile.linux 
echo ""

echo "#################### DATAFITTERLIB LIBRARY ######################"
echo "Compiling Library DataFitterLib..."
cd ../../DataFitterLib/src 
make -f ./Makefile.linux 
echo ""

echo "#################### EXCEPTIONS LIBRARY ######################"
echo "Compiling Library Exceptions..."
cd ../../Exceptions/src 
make -f Makefile.linux 
echo ""

echo "####################### GENERIC BENDER LIBRARY #######################"
echo "Compiling Library Generic Bender..."
cd ../../GenericBender/src
make -f ./Makefile.linux 
echo ""

echo "############## GRATING MONOCHROMATOR LIBRARY #################"
echo "Compiling Library GratingMonochromator..."
cd ../../GratingMonochromator/src 
make -f Makefile.linux 
echo ""

#echo "######################## GSL LIBRARY #########################"
#echo "Compiling Library GSL..."
#cd ../../GSL/src
#make -f Makefile.linux 
#echo ""

echo "################### INTERPOLATOR LIBRARY #####################"
echo "Compiling Library Interpolator..."
cd ../../Interpolator/src 
make -f Makefile.linux 
echo ""

echo "################### MIRROR LIBRARY #####################"
echo "Compiling Library Mirror..."
cd ../../Mirror/src 
make -f Makefile.linux
echo ""

echo "################## MONOCHROMATOR LIBRARY #####################"
echo "Compiling Library Monochromator..."
cd ../../Monochromator/src 
make -f Makefile.linux 
echo ""

echo "###################### UTILS LIBRARY #########################"
echo "Compiling Library Utils..."
cd ../../Utils/src 
make -f Makefile.linux 
echo ""

cd ../.. 
