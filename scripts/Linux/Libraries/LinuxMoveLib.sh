#!/bin/bash
 
cp -f --reply=yes BeamLineEnergy/lib/*.a           /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/BeamLineEnergy/lib 
cp -f --reply=yes DataFitterLib/lib/*.a               /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/DataFitterLib/lib 
cp -f --reply=yes Exceptions/lib/*.a               /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Exceptions/lib 
cp -f --reply=yes GenericBender/lib/*.a            /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/GenericBender/lib
cp -f --reply=yes GratingMonochromator/lib/*.a     /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/GratingMonochromator/lib
cp -f --reply=yes Interpolator/lib/*.a             /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Interpolator/lib 
cp -f --reply=yes Mirror/lib/*.a                   /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Mirror/lib 
cp -f --reply=yes Monochromator/lib/*.a            /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Monochromator/lib 
cp -f --reply=yes Utils/lib/*.a                    /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Utils/lib 

cp -f --reply=yes BeamLineEnergy/include/*.h           /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/BeamLineEnergy/include 
cp -f --reply=yes DataFitterLib/include/*.h               /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/DataFitterLib/include 
cp -f --reply=yes Exceptions/include/*.h               /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Exceptions/include 
cp -f --reply=yes GenericBender/include/*.h            /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/GenericBender/include
cp -f --reply=yes GratingMonochromator/include/*.h     /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/GratingMonochromator/include
cp -f --reply=yes Interpolator/include/*.h             /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Interpolator/include 
cp -f --reply=yes Mirror/include/*.h                   /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Mirror/include
cp -f --reply=yes Monochromator/include/*.h            /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Monochromator/include 
cp -f --reply=yes Utils/include/*.h                    /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Utils/include 


cp -f --reply=yes BeamLineEnergy/doc/doxygen/html/*           /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/BeamLineEnergy/doc/Doxygen/html 
cp -f --reply=yes DataFitterLib/doc/doxygen/html/*              /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/DataFitterLib/doc/Doxygen/html 
cp -f --reply=yes Exceptions/doc/doxygen/html/*               /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Exceptions/doc/Doxygen/html 
cp -f --reply=yes GenericBender/doc/doxygen/html/*            /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/GenericBender/doc/Doxygen/html
cp -f --reply=yes GratingMonochromator/doc/doxygen/html/*     /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/GratingMonochromator/doc/Doxygen/html
cp -f --reply=yes Interpolator/doc/doxygen/html/*             /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Interpolator/doc/Doxygen/html 
cp -f --reply=yes Mirror/doc/doxygen/html/*                   /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Mirror/doc/Doxygen/html
cp -f --reply=yes Monochromator/doc/doxygen/html/*            /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Monochromator/doc/Doxygen/html 
cp -f --reply=yes Utils/doc/doxygen/html/*                    /home/ica3/langlois/SourceCVS/tango-soleil-rhel-gcc344/sw-support/Utils/doc/Doxygen/html 


