#!/bin/bash

echo "##############################################################"
echo "Cleaning Device Beam Line Energy Proxima..."
cd BeamLineEnergyProxima/src
make -f ./Makefile.linux clean 
echo ""

echo "##############################################################"
echo "Cleaning Device Beam Line Energy Tempo..."
cd ../../BeamLineEnergyTempo/src 
make -f ./Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device Blade Generic Bender..."
cd ../../BladeGenericBender/src 
make -f ./Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device DataFitter..."
cd ../../DataFitter/src 
make -f ./Makefile.linux clean
echo ""

echo "##############################################"
echo "Cleaning Device GenericMirror..."
cd ../../GenericMirror/src
make -f ./Makefile.linux  clean 
echo ""

echo "##############################################"
echo "Cleaning Device MechanicalGenericBender..."
cd ../../MechanicalGenericBender/src
make -f ./Makefile.linux  clean 
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorCassiopee..."
cd ../../MonochromatorCassiopee/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorCristal..."
cd ../../MonochromatorCristal/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorDesir..."
cd ../../MonochromatorDesir/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorDiffabs..."
cd ../../MonochromatorDiffabs/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorMars..."
cd ../../MonochromatorMars/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorOde..."
cd ../../MonochromatorOde/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorProxima..."
cd ../../MonochromatorProxima/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorSamba..."
cd ../../MonochromatorSamba/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorSwing..."
cd ../../MonochromatorSwing/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device MonochromatorTempo..."
cd ../../MonochromatorTempo/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device OndulatorMask..."
cd ../../OndulatorMask/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device TraitPointPlan..."
cd ../../TraitPointPlan/src 
make -f Makefile.linux clean
echo ""

echo "##############################################################"
echo "Cleaning Device Triple Generic Mirror..."
cd ../../TripleGenericMirror/src 
make -f ./Makefile.linux clean
echo ""

cd ../.. 

echo "##############################################################"
echo "Removing *.*~ in src..."
rm -f BeamLineEnergyProxima/src/*.*~
rm -f BeamLineEnergyTempo/src/*.*~
rm -f BladeGenericBender/src/*.*~
rm -f DataFitter/src/*.*~
rm -f GenericMirror/src/*.*~
rm -f MechanicalGenericBender/src/*.*~
rm -f MonochromatorCassiopee/src/*.*~
rm -f MonochromatorCristal/src/*.*~
rm -f MonochromatorDesir/src/*.*~
rm -f MonochromatorDiffabs/src/*.*~
rm -f MonochromatorMars/src/*.*~
rm -f MonochromatorOde/src/*.*~
rm -f MonochromatorProxima/src/*.*~
rm -f MonochromatorSamba/src/*.*~
rm -f MonochromatorSwing/src/*.*~
rm -f MonochromatorTempo/src/*.*~
rm -f OndulatorMask/src/*.*~
rm -f TraitPointPlan/src/*.*~
rm -f TripleGenericMirror/src/*.*~
echo "##############################################################"
