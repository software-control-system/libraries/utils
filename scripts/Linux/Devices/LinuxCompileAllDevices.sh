#!/bin/bash

echo "##############################################################"
echo "Compiling Device BEAM LINE ENERGY PROXIMA..."
cd BeamLineEnergyProxima/src
make -f ./Makefile.linux 
echo ""

echo "##############################################################"
echo "Compiling Device BEAM LINE ENERGY TEMPO..."
cd ../../BeamLineEnergyTempo/src 
make -f ./Makefile.linux
echo ""

echo "#################### DEVICE BLADE GENERIC BENDER #######################"
echo "Compiling Device BladeGenericBender..."
cd ../../BladeGenericBender/src 
make -f ./Makefile.linux 
echo ""

echo "#################### DEVICE DATAFITTER #######################"
echo "Compiling Device DataFitter..."
cd ../../DataFitter/src 
make -f ./Makefile.linux 
echo ""

echo "#################### DEVICE GENERIC MIRROR #######################"
echo "Compiling Device Generic Mirror..."
cd ../../GenericMirror/src 
make -f ./Makefile.linux 
echo ""

echo "#################### DEVICE MECHANICAL GENERIC BENDER #######################"
echo "Compiling Device Mechanical Generic Bender..."
cd ../../MechanicalGenericBender/src 
make -f ./Makefile.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR CASSIOPEE #################"
echo "Compiling Device MonochromatorCassiopee..."
cd ../../MonochromatorCassiopee/src 
make -f Makefile.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR CRISTAL #################"
echo "Compiling Device MonochromatorCristal..."
cd ../../MonochromatorCristal/src 
make -f Makefile.linux 
echo ""

echo "############### DEVICE MONOCHROMATOR DESIR ###################"
echo "Compiling Device MonochromatorDesir..."
cd ../../MonochromatorDesir/src 
make -f Makefile.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR DIFFABS ###################"
echo "Compiling Device MonochromatorDiffabs..."
cd ../../MonochromatorDiffabs/src 
make -f Makefile.linux
echo ""

echo "############# DEVICE MONOCHROMATOR MARS ###################"
echo "Compiling Device MonochromatorMars..."
cd ../../MonochromatorMars/src 
make -f Makefile.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR ODE ###################"
echo "Compiling Device MonochromatorOde..."
cd ../../MonochromatorOde/src 
make -f Makefile.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR PROXIMA ###################"
echo "Compiling Device MonochromatorProxima..."
cd ../../MonochromatorProxima/src 
make -f Makefile.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR SAMBA ###################"
echo "Compiling Device MonochromatorSamba..."
cd ../../MonochromatorSamba/src 
make -f MakefileDev.linux 
echo ""

echo "############# DEVICE MONOCHROMATOR SWING ###################"
echo "Compiling Device MonochromatorSwing..."
cd ../../MonochromatorSwing/src 
make -f MakefileDev.linux 
echo ""

echo "############### DEVICE MONOCHROMATOR TEMPO ###################"
echo "Compiling Device MonochromatorTempo..."
cd ../../MonochromatorTempo/src 
make -f Makefile.linux 
echo ""

echo "############### DEVICE ONDULATOR MASK ###################"
echo "Compiling Device OndulatorMask..."
cd ../../OndulatorMask/src 
make -f MakefileDev.linux 
echo ""

echo "################### DEVICE TRAITPOINTPLAN ####################"
echo "Compiling Device TraitPointPlan..."
cd ../../TraitPointPlan/src 
make -f Makefile.linux 
echo ""

echo "################### DEVICE TRIPLE GENERIC MIRROR ####################"
echo "Compiling Device Triple generic Mirror..."
cd ../../TripleGenericMirror/src 
make -f Makefile.linux 
echo ""


cd ../.. 
