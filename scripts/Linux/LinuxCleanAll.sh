#!/bin/bash

echo "####################### CLEANING LIBRARY #######################"
echo ""
cd Libraries/
LinuxCleanAllLibs.sh 
echo ""

echo "####################### CLEANING DEVICES #######################"
echo ""
cd ../Devices/ 
LinuxCleanAllDevices.sh
echo ""

cd ..