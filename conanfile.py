from conan import ConanFile

class utilsRecipe(ConanFile):
    name = "utils"
    version = "1.2.5"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Nourredine"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/utils"
    description = "Utils library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"
