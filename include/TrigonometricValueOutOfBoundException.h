/// TrigonometricValueOutOfBoundException.h: interface for the TrigonometricValueOutOfBoundException class

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRIGONOMETRICVALUEOUTOFBoundEXCEPTION_H__60B59B02_B8F5_422A_90AC_2E4084C6F8F0__INCLUDED_)
#define AFX_TRIGONOMETRICVALUEOUTOFBoundEXCEPTION_H__60B59B02_B8F5_422A_90AC_2E4084C6F8F0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArithmeticException.h"
//#include <string>
#include "Tools.h"
//using namespace std;

///This class allows to manage exceptions due to a value out of Bound for a trigonometric function.
//##ModelId=43834ABD0396
class TrigonometricValueOutOfBoundException : public ArithmeticException  
{
public:
	//##ModelId=43834ABD0398
	TrigonometricValueOutOfBoundException() throw();
	//##ModelId=43834ABD03A6
	TrigonometricValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax,std::string sFunctionName) throw();
	//##ModelId=43834ABD03AC
	TrigonometricValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax,std::string sFunctionName,std::string sOrigin,std::string sFilename,unsigned int iLineNumber) throw();
	//##ModelId=43834ABD03BB
	virtual ~TrigonometricValueOutOfBoundException() throw();
	
	//##ModelId=43834ABD03BD
	virtual std::string makeDescription();
	//##ModelId=43834ABD03BF
	virtual std::string makeReason();
	//##ModelId=43834ABD03C1
	virtual std::string makeOrigin(std::string sOrigin);
private :
	//##ModelId=43834ABD03C8
	std::string _sElementName;
	//##ModelId=43834ABD03D5
	double _dValue;
	//##ModelId=43834ABD03CC
	double _dValueMin;
	//##ModelId=43834ABD03CD
	double _dValueMax;

	//##ModelId=43834ABD03E5
	std::string _sFunctionName;
};

#endif // !defined(AFX_TRIGONOMETRICVALUEOUTOFBoundEXCEPTION_H__60B59B02_B8F5_422A_90AC_2E4084C6F8F0__INCLUDED_)
