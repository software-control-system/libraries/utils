#if !defined CONSTANT_H
#define CONSTANT_H

#include <string>
using namespace std;

namespace MATH_CONSTANT
{
	static const double PI						= 3.1415926535897932384626433832795;
	static const double SQRT2					= 1.4142135623730950488016887242096;
}

namespace PHYSICAL_CONSTANT
{
	static const double PLANCK_CONSTANT			= 6.62617E-34;
	static const double CELERITY				= 299792458.;
}

namespace MONO_CONSTANT
{
	static const double THETA_BRAGG_CONSTANT			= 12.39841857;
	static const double SI111_MAILLE_PARAMETER_CONSTANT	= 5.4309;
}

#endif

