// InfinityException.h: interface for the InfinityException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INFINITYEXCEPTION_H__67A4495D_93D4_4FBD_BA07_487D6F1BFB3D__INCLUDED_)
#define AFX_INFINITYEXCEPTION_H__67A4495D_93D4_4FBD_BA07_487D6F1BFB3D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArithmeticException.h"
//#include <string>

class InfinityException : public ArithmeticException  
{
public:
	InfinityException() throw();
	
	InfinityException(	std::string sExpressionToCompute,
						std::string sZeroValue) throw();

	InfinityException(	std::string sExpressionToCompute,
						std::string sInfinityValue,
						std::string sOrigin,
						std::string sFileName,
						unsigned int iLineNumber) throw();

	virtual ~InfinityException() throw();

	virtual std::string makeDescription();
	virtual std::string makeReason();
	virtual std::string makeOrigin(std::string sOrigin);

private:

	std::string _sExpressionToCompute;
	std::string _sInfinityValue;
};

#endif // !defined(AFX_INFINITYEXCEPTION_H__67A4495D_93D4_4FBD_BA07_487D6F1BFB3D__INCLUDED_)
