/// IndexOutOfBoundException.h: interface for the IndexOutOfBoundException class

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INDEXOUTOFBoundEXCEPTION_H__39E80036_595B_4088_A11B_D105BA6F99DF__INCLUDED_)
#define AFX_INDEXOUTOFBoundEXCEPTION_H__39E80036_595B_4088_A11B_D105BA6F99DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Bound.h"
#include "OutOfBoundException.h"

/// The index out of Bound exception class manage errors due to a bad given index
/// to access an array
//##ModelId=43834AC0028C
class IndexOutOfBoundException : public OutOfBoundException  
{
public:
	//##ModelId=43834AC0029D
	IndexOutOfBoundException() throw();
	//##ModelId=43834AC0029E
	IndexOutOfBoundException(std::string sElementName,long iIndex,long iIndexMin,long iIndexMax) throw();
	//##ModelId=43834AC002A3
	IndexOutOfBoundException(std::string sElementName,long iIndex,long iIndexMin,long iIndexMax,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();
	
	//##ModelId=43834AC002B1
	virtual ~IndexOutOfBoundException() throw();

	//##ModelId=43834AC002B3
	virtual std::string makeDescription();
	//##ModelId=43834AC002B5
	virtual std::string makeReason();
	//##ModelId=43834AC002BB
	virtual std::string makeOrigin(std::string sOrigin);
private :
	//##ModelId=43834AC002BF
	std::string _sElementName;
	//##ModelId=43834AC002CC
	long _iIndex;
	//##ModelId=43834AC002C3
	long _iIndexMin;
	//##ModelId=43834AC002CB
	long _iIndexMax;

};

#endif // !defined(AFX_INDEXOUTOFBoundEXCEPTION_H__39E80036_595B_4088_A11B_D105BA6F99DF__INCLUDED_)
