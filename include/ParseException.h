// ParseException.h: interface for the ParseException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PARSEEXCEPTION_H__D66B23F4_095E_4EA1_8AB9_93C79E3602EC__INCLUDED_)
#define AFX_PARSEEXCEPTION_H__D66B23F4_095E_4EA1_8AB9_93C79E3602EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileException.h"
//#include <string.h>

//using namespace std;

///This class is used to manage an exception during the parsing of a file or a buffer.
//##ModelId=43834ABF00B8
class ParseException : public FileException  
{
public:
	//##ModelId=43834ABF00BA
	ParseException() throw();
	//##ModelId=43834ABF00BB
	ParseException(std::string sElementName) throw();
	//##ModelId=43834ABF00BD
	ParseException(std::string sElementName,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();
	//##ModelId=43834ABF00C8
	virtual ~ParseException() throw();

	//##ModelId=43834ABF00CA
	virtual std::string makeDescription();
	//##ModelId=43834ABF00CC
	virtual std::string makeReason();
	//##ModelId=43834ABF00CE
	virtual std::string makeOrigin(std::string sOrigin);

private:
	//##ModelId=43834ABF00D8
	std::string _sElementName;
};

#endif // !defined(AFX_PARSEEXCEPTION_H__D66B23F4_095E_4EA1_8AB9_93C79E3602EC__INCLUDED_)
