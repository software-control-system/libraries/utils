// FileException.h: interface for the FileException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILEEXCEPTION_H__69CE36FD_20F9_4382_BDBC_F256E739EC06__INCLUDED_)
#define AFX_FILEEXCEPTION_H__69CE36FD_20F9_4382_BDBC_F256E739EC06__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"
//#include <string>
//using namespace std;

///This class manage file exception
//##ModelId=43834AC101E2
class FileException : public Exception  
{
public:
	//##ModelId=43834AC101F1
	FileException() throw();
	//##ModelId=43834AC101F2
	virtual ~FileException() throw();
		
	//##ModelId=43834AC101F4
	virtual std::string	makeDescription()=0;
	//##ModelId=43834AC101F6
	virtual std::string	makeReason()=0;
	//##ModelId=43834AC101F8
	virtual std::string	makeOrigin(std::string sOrigin)=0;

	//##ModelId=43834AC101FB
	virtual std::string	getNameOfTheFile();
	//##ModelId=43834AC10200
	virtual void	setNameOfTheFile(std::string sNameOfTheFile);

private:
	//##ModelId=43834AC10210
	std::string _sNameOfTheFile;
};

#endif // !defined(AFX_FILEEXCEPTION_H__69CE36FD_20F9_4382_BDBC_F256E739EC06__INCLUDED_)
