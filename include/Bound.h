// Bound.h: interface for the Bound class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOUND_H__2D755A04_E20D_4C19_B680_478AF1F0F40B__INCLUDED_)
#define AFX_BOUND_H__2D755A04_E20D_4C19_B680_478AF1F0F40B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <yat/utils/Logging.h>
#include <string>
//using namespace std;

///This class is used to define boundary conditions.
//##ModelId=43872EC80065
class Bound  
{
public:
	//##ModelId=43872EC80066
	Bound();
	//##ModelId=43872EC80067
	Bound(std::string sName,double dLowerBound,double dUpperBound);
	//##ModelId=43872EC8006B
	virtual ~Bound();

	//##ModelId=43872EC8006D
	inline void setName(std::string sName);
	//##ModelId=43872EC80076
	inline std::string getName() const;

	//##ModelId=43872EC80078
	virtual void	setLowerBound(double dLowerBound);
	//##ModelId=43872EC8007B
	virtual double	getLowerBound() const;

	//##ModelId=43872EC8007D
	virtual void	setUpperBound(double dUpperBound);
	//##ModelId=43872EC80085
	virtual double	getUpperBound() const;
	
	//##ModelId=43872EC80087
	void setBound(Bound* mBound);
	//##ModelId=43872EC80089
	void setBound(std::string sName,double dLowerBound,double dUpperBound);
	
	//##ModelId=439D88340256
	bool isInBound(double dValue);
	//##ModelId=439D88340276
	bool isStricklyInBound(double dValue) ;

	//##ModelId=439D88340285
	Bound& operator=(Bound* pBound);
		

	//##ModelId=43872EC8008D
	Bound(const Bound& mBound); 


	void printInfos();

private:
	//##ModelId=43872EC80095
	std::string _sName;
	//##ModelId=43872EC80099
	double _dLowerBound;
	//##ModelId=43872EC800E3
	double _dUpperBound;
};

#endif // !defined(AFX_BOUND_H__2D755A04_E20D_4C19_B680_478AF1F0F40B__INCLUDED_)
