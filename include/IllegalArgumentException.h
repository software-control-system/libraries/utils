/// IllegalArgumentException.h: interface for the IllegalArgumentExcpetion class

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ILLEGALARGUMENTEXCEPTION_H__B28FD4EE_FE00_4D03_B1B0_09C588E690F1__INCLUDED_)
#define AFX_ILLEGALARGUMENTEXCEPTION_H__B28FD4EE_FE00_4D03_B1B0_09C588E690F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"

//#include <string>
#include <vector>
#include "stdarg.h"
/// This class is used to manage an Illegal Argument Exception
/// This exception is thrown when a bad object argument type is done to a function
/// NOT YET IMPLEMENTED !!!
class IllegalArgumentException : public Exception  
{
public:

	IllegalArgumentException() throw();

	virtual ~IllegalArgumentException() throw();

	virtual std::string makeDescription();

	virtual std::string makeReason();
	
	virtual std::string makeOrigin(std::string sOrigin);

	IllegalArgumentException(std::string sElementName,
							   std::string sOrigin,
							   std::string sFileName,
							   unsigned int iLineNumber,
							   double dIndex,
							   long iNumberOfArguments,
							   ...);


private :

	std::string			_sElementName;
	double				_dIndex;
	long				_iNbArguments;
	std::vector<double>	_vArgumentsList;
};

#endif // !defined(AFX_ILLEGALARGUMENTEXCEPTION_H__B28FD4EE_FE00_4D03_B1B0_09C588E690F1__INCLUDED_)
