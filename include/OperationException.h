// OperationException.h: interface for the OperationException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OPERATIONEXCEPTION_H__2F2A8F9F_E28C_46B5_B5FC_E4A4CC391EEA__INCLUDED_)
#define AFX_OPERATIONEXCEPTION_H__2F2A8F9F_E28C_46B5_B5FC_E4A4CC391EEA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"

/// This class is an abstract class for operation exception
class OperationException : public Exception  
{
public:
    
	OperationException() throw();
	virtual ~OperationException() throw();

	virtual std::string makeDescription()=0;
	virtual std::string makeReason()=0;
	virtual std::string makeOrigin(std::string sOrigin)=0;
};

#endif // !defined(AFX_OPERATIONEXCEPTION_H__2F2A8F9F_E28C_46B5_B5FC_E4A4CC391EEA__INCLUDED_)
