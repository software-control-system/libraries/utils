/// ValueOutOfBoundException.h: interface for the ValueOutOfBoundException class.

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VALUEOUTOFBoundEXCEPTION_H__245478B9_402C_4A56_AB26_889E54AD0CC5__INCLUDED_)
#define AFX_VALUEOUTOFBoundEXCEPTION_H__245478B9_402C_4A56_AB26_889E54AD0CC5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <string>
#include "OutOfBoundException.h"
#include "Bound.h"
/// This class is used to managed a value out of the allowed bound
//##ModelId=43834ABD02CB
class ValueOutOfBoundException : public OutOfBoundException  
{
public:
	//##ModelId=43834ABD02CD
	ValueOutOfBoundException() throw();

	//##ModelId=438739560186
	ValueOutOfBoundException(std::string sElementName,double dValue,Bound* mBound) throw();

	//##ModelId=43834ABD02CE
	ValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax) throw();
	//##ModelId=43834ABD02DD
	ValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();
	//##ModelId=43834ABD02E5
	virtual ~ValueOutOfBoundException() throw();
	
	//##ModelId=43834ABD02EA
	virtual std::string makeDescription();
	//##ModelId=43834ABD02EC
	virtual std::string makeReason();
	//##ModelId=43834ABD02EE
	virtual std::string makeOrigin(std::string sOrigin);
private :
	//##ModelId=43834ABD02F2
	std::string _sElementName;
	//##ModelId=43834ABD02FC
	double _dValue;
	//##ModelId=43834ABD02FA
	double _dValueMin;
	//##ModelId=43834ABD02FB
	double _dValueMax;

};

#endif // !defined(AFX_VALUEOUTOFBoundEXCEPTION_H__245478B9_402C_4A56_AB26_889E54AD0CC5__INCLUDED_)
