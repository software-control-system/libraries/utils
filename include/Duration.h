// Duration.h: interface for the Duration class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DURATION_H__95A8A4BA_3CFB_4ABC_8C48_495328EA1336__INCLUDED_)
#define AFX_DURATION_H__95A8A4BA_3CFB_4ABC_8C48_495328EA1336__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef WIN32
	#pragma once
	#include <windows.h>
#else
	#include <sys/time.h>
	#define TIMEVAL struct timeval 
	#define TIMEVAL struct timeval 
	
	#ifndef NULL
		#define NULL 0
	#endif

	#define	GET_TIME(T) gettimeofday(&T,NULL)
	#define	ELAPSED_TIME_SEC(B, A) \
 	 	static_cast<double>((A.tv_sec - B.tv_sec) + (1.E-6 * (A.tv_usec - B.tv_usec))) // B = start; A = end #endif
#endif

class Duration  
{
public:
	Duration();
	virtual ~Duration() {};

	void Start(void);
	void Stop(void);
	double GetDuration(void) const;
	double GetDurationInMs(void) const;

protected:

#ifdef WIN32
	LARGE_INTEGER m_liStart;
	LARGE_INTEGER m_liStop;

	LONGLONG m_llFrequency;
	LONGLONG m_llCorrection;
#else
	TIMEVAL 	m_liStart;
	TIMEVAL 	m_liStop;
//	LONGLONG 	m_llCorrection;
#endif
};


inline Duration::Duration(void)
{
#ifdef WIN32
	LARGE_INTEGER liFrequency;

	QueryPerformanceFrequency(&liFrequency);
	m_llFrequency = liFrequency.QuadPart;
	
	// Calibration
	Start();
	Stop();

	m_llCorrection = m_liStop.QuadPart-m_liStart.QuadPart;
#else
	// Calibration
//	Start();
//	Stop();

//	m_llCorrection = GetDuration();
#endif
}

inline void Duration::Start(void)
{

#ifdef WIN32
	// Ensure we will not be interrupted by any other thread for a while
	Sleep(0);
	QueryPerformanceCounter(&m_liStart);
#else
	GET_TIME(m_liStart);
#endif


}

inline void Duration::Stop(void)
{
#ifdef WIN32
	QueryPerformanceCounter(&m_liStop);
#else
	GET_TIME(m_liStop);
#endif

}

inline double Duration::GetDuration(void) const
{
#ifdef WIN32
	return (double)(m_liStop.QuadPart-m_liStart.QuadPart-m_llCorrection)*1000000.0 / m_llFrequency; 
#else
	return ELAPSED_TIME_SEC(m_liStart, m_liStop);
#endif
}

inline double Duration::GetDurationInMs(void) const
{
#ifdef WIN32
	return (double)(m_liStop.QuadPart-m_liStart.QuadPart-m_llCorrection)*1000000.0 / m_llFrequency /1000.0; 
#else
	return ELAPSED_TIME_SEC(m_liStart, m_liStop)*1000.0;  ///not sure !!!
#endif
}


#endif // !defined(AFX_DURATION_H__95A8A4BA_3CFB_4ABC_8C48_495328EA1336__INCLUDED_)
