// SquareRootException.h: interface for the SquareRootException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SQUAREROOTEXCEPTION_H__39C90240_1ED1_47F5_B7FF_563D931A5028__INCLUDED_)
#define AFX_SQUAREROOTEXCEPTION_H__39C90240_1ED1_47F5_B7FF_563D931A5028__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArithmeticException.h"

///This class allows to manage exceptions due to a negative value inside a square root.
//##ModelId=43834ABD0396
class SquareRootException : public ArithmeticException  
{
public:
	//##ModelId=43834ABD0398
	SquareRootException() throw();
	//##ModelId=43834ABD03A6
	SquareRootException(std::string sElementName,double dValue) throw();
	//##ModelId=43834ABD03AC
	SquareRootException(std::string sElementName,double dValue,std::string sOrigin,std::string sFilename,unsigned int iLineNumber) throw();
	//##ModelId=43834ABD03BB
	virtual ~SquareRootException() throw();
	
	//##ModelId=43834ABD03BD
	virtual std::string makeDescription();
	//##ModelId=43834ABD03BF
	virtual std::string makeReason();
	//##ModelId=43834ABD03C1
	virtual std::string makeOrigin(std::string sOrigin);
private :
	//##ModelId=43834ABD03C8
	std::string _sElementName;
	//##ModelId=43834ABD03D5
	double _dValue;
};

#endif // !defined(AFX_SQUAREROOTEXCEPTION_H__39C90240_1ED1_47F5_B7FF_563D931A5028__INCLUDED_)
