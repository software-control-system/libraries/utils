/// ArithmeticException.h: interface for the ArithmeticException class

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARITHMETICEXCEPTION_H__E829BCCE_4E01_4F6F_B85D_D56FBFF822B9__INCLUDED_)
#define AFX_ARITHMETICEXCEPTION_H__E829BCCE_4E01_4F6F_B85D_D56FBFF822B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"

/// This class is an abstract class for arithmetic like exception
//##ModelId=43834AC30144
class ArithmeticException : public Exception  
{
public:
	//##ModelId=43834AC30146
	ArithmeticException() throw();
	//##ModelId=43834AC30147
	virtual ~ArithmeticException() throw();

	//##ModelId=43834AC30153
	virtual std::string makeDescription()=0;
	//##ModelId=43834AC30155
	virtual std::string makeReason()=0;
	//##ModelId=43834AC30157
	virtual std::string makeOrigin(std::string sOrigin)=0;
};

#endif // !defined(AFX_ARITHMETICEXCEPTION_H__E829BCCE_4E01_4F6F_B85D_D56FBFF822B9__INCLUDED_)
