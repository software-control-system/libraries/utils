/// DivideByZeroException.h: interface for the DivideByZeroException class

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIVIDEBYZEROEXCEPTION_H__9CF48763_105C_4B16_BAEB_3582287654E7__INCLUDED_)
#define AFX_DIVIDEBYZEROEXCEPTION_H__9CF48763_105C_4B16_BAEB_3582287654E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArithmeticException.h"
//#include <string>

//using namespace std;

/// This class is used to manage a Divide By Zero Exception
//##ModelId=43834AC102AB
class DivideByZeroException : public ArithmeticException  
{
public:
	//##ModelId=43834AC102BC
	DivideByZeroException() throw();
	//##ModelId=43834AC102BD
	DivideByZeroException(std::string sExpressionToCompute,std::string sZeroValue) throw();
	//##ModelId=43834AC102CC
	DivideByZeroException(std::string sExpressionToCompute,std::string sZeroValue,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();

	//##ModelId=43834AC102DD
	virtual ~DivideByZeroException() throw();

	//##ModelId=43834AC102EA
	virtual std::string makeDescription();
	//##ModelId=43834AC102EC
	virtual std::string makeReason();
	//##ModelId=43834AC102FA
	virtual std::string makeOrigin(std::string sOrigin);

private:
	//##ModelId=43834AC1030A
	std::string _sExpressionToCompute;
	//##ModelId=43834AC1031A
	std::string _sZeroValue;
};

#endif // !defined(AFX_DIVIDEBYZEROEXCEPTION_H__9CF48763_105C_4B16_BAEB_3582287654E7__INCLUDED_)
