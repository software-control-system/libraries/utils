// NoSuchElementException.h: interface for the NoSuchElementException class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NOSUCHELEMENTEXCEPTION_H__7E54F31C_72B7_4F4F_BB2D_DC217AF32651__INCLUDED_)
#define AFX_NOSUCHELEMENTEXCEPTION_H__7E54F31C_72B7_4F4F_BB2D_DC217AF32651__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"
//#include <string>
//using namespace std;

///This class is used to manage exception due to a element which can not be found
//##ModelId=43834ABF0115
class NoSuchElementException : public Exception  
{
public:
	//##ModelId=43834ABF0117
	NoSuchElementException() throw();
	//##ModelId=43834ABF0125
	NoSuchElementException(std::string sElementName) throw();
	//##ModelId=43834ABF0127
	NoSuchElementException(std::string sElementName,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();
	//##ModelId=43834ABF012C
	virtual ~NoSuchElementException() throw();

	//##ModelId=43834ABF012E
	virtual std::string makeDescription();
	//##ModelId=43834ABF0130
	virtual std::string makeReason();
	//##ModelId=43834ABF0136
	virtual std::string makeOrigin(std::string sOrigin);
private :
	//##ModelId=43834ABF013A
	std::string _sElementName;
};

#endif // !defined(AFX_NOSUCHELEMENTEXCEPTION_H__7E54F31C_72B7_4F4F_BB2D_DC217AF32651__INCLUDED_)
