// FileNotFoundException.h: interface for the FileNotFoundException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILENOTFOUNDEXCEPTION_H__EBCFF9F2_9B57_47D7_AAB0_CE6E5A4E3798__INCLUDED_)
#define AFX_FILENOTFOUNDEXCEPTION_H__EBCFF9F2_9B57_47D7_AAB0_CE6E5A4E3798__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileException.h"
//#include <string>

//using namespace std;

///This class is used to manage file ot found exception
///This exception is thrown when an attempt to open the file denoted by a specified pathname has failed.
//##ModelId=43834AC101C1
class FileNotFoundException : public FileException  
{
public:
	//##ModelId=43834AC101C3
	FileNotFoundException() throw();
	//##ModelId=43834AC101D1
	FileNotFoundException(std::string sNameOfTheFile,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();
	//##ModelId=43834AC101D6
	virtual ~FileNotFoundException() throw();

	//##ModelId=43834AC101D8
	virtual std::string makeDescription();
	//##ModelId=43834AC101DA
	virtual std::string makeReason();
	//##ModelId=43834AC101DC
	virtual std::string makeOrigin(std::string sOrigin);
};

#endif // !defined(AFX_FILENOTFOUNDEXCEPTION_H__EBCFF9F2_9B57_47D7_AAB0_CE6E5A4E3798__INCLUDED_)
