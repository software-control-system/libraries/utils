/// Exception.h: interface for the Exception class

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXCEPTION_H__7BB2B20F_0FAE_4A97_8C6A_BBC8F4F57301__INCLUDED_)
#define AFX_EXCEPTION_H__7BB2B20F_0FAE_4A97_8C6A_BBC8F4F57301__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <exception>
#include <string>
//using namespace std;

#include "Tools.h"

/// The main abstract class to manage all the library exceptions
//##ModelId=43834AC1021F
class Exception : public std::exception  
{
public:
	//##ModelId=43834AC10221
	Exception() throw();
	//##ModelId=43834AC10222
	virtual ~Exception() throw();

	//##ModelId=43834AC1022F
	virtual std::string makeDescription()=0;
	//##ModelId=43834AC10231
	virtual std::string makeReason()=0;
	//##ModelId=43834AC10233
	virtual std::string makeOrigin(std::string sOrigin)=0;

	//##ModelId=43834AC10236
	virtual std::string getReason();
	//##ModelId=43834AC10238
	virtual std::string getDescription();
	//##ModelId=43834AC1023E
	virtual std::string getOrigin();
	//##ModelId=43834AC10240
	virtual unsigned int getLineNumber();
	//##ModelId=43834AC10242
	virtual std::string getFileName();
	//##ModelId=43834AC10244
	virtual std::string getType();

	//##ModelId=43834AC10246
	virtual void setReason(std::string sReason);
	//##ModelId=43834AC10249
	virtual void setDescription(std::string sDescription);
	//##ModelId=43834AC1024F
	virtual void setOrigin(std::string sOrigin);
	//##ModelId=43834AC10252
	virtual void setLineNumber(unsigned int iLineNumber);
	//##ModelId=43834AC10255
	virtual void setFileName(std::string sFileName);
	//##ModelId=43834AC10258
	virtual void setType(std::string sType);

private:
	
	//##ModelId=43834AC10260
	std::string _sReason;
	//##ModelId=43834AC10265
	std::string _sDescription;
	//##ModelId=43834AC1026E
	std::string _sOrigin;
	//##ModelId=43834AC10273
	std::string _sType;
	//##ModelId=43834AC1027C
	unsigned int _iLineNumber;
	//##ModelId=43834AC1027E
	std::string _sFileName;
};

#endif // !defined(AFX_EXCEPTION_H__7BB2B20F_0FAE_4A97_8C6A_BBC8F4F57301__INCLUDED_)
