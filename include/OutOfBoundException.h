// OutOfBoundException.h: interface for the OutOfBoundException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OUTOFBoundEXCEPTION_H__8CCE8B74_102C_4DD3_8B9D_D82141B435BF__INCLUDED_)
#define AFX_OUTOFBoundEXCEPTION_H__8CCE8B74_102C_4DD3_8B9D_D82141B435BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"

/// This class is an abstract class for out of bound exception
//##ModelId=43834ABF00DC
class OutOfBoundException : public Exception  
{
public:
	//##ModelId=43834ABF00E8
	OutOfBoundException() throw();
	//##ModelId=43834ABF00E9
	virtual ~OutOfBoundException() throw();

	//##ModelId=438433EE00ED
	virtual std::string	makeDescription()=0;
	//##ModelId=438433EE010C
	virtual std::string	makeReason()=0;
	//##ModelId=438433EE012D
	virtual std::string	makeOrigin(std::string sOrigin)=0;

};

#endif // !defined(AFX_OUTOFBoundEXCEPTION_H__8CCE8B74_102C_4DD3_8B9D_D82141B435BF__INCLUDED_)
