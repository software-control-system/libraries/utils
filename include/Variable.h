// Variable.h: interface for the Variable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VARIABLE_H__04D26E63_7F48_4183_A917_EF05AB3C9CCD__INCLUDED_)
#define AFX_VARIABLE_H__04D26E63_7F48_4183_A917_EF05AB3C9CCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <yat/utils/Logging.h>
#include <string>
//using namespace std;
#include "Bound.h"
#include "ValueOutOfBoundException.h"
#include "NullPointerException.h"

//template <class ValueType>
//##ModelId=43834ABD027E
///This class is used to define a Variable object 
class Variable  
{
public:

	//##ModelId=43834ABD027F
	Variable();
	//##ModelId=43834ABD0280
	Variable(std::string sName,double dLowerVariableBound,double dUpperVariableBound,double dReferenceValue=0.0);
	
	//##ModelId=43834ABD0285
	virtual ~Variable();

	//##ModelId=43834ABD028D
	virtual void	setValue(double dNewValue); //throw (ValueOutOfBoundException);

	virtual void	setUncheckValue(double dNewValue);
	
	//##ModelId=43834ABD0290
	virtual double	getValue() const;
//	virtual double	getNonCorrectedValue() const;

	//##ModelId=43834ABD0292
	virtual void	setName(std::string sNewName);
	//##ModelId=43834ABD0295
	virtual std::string	getName() const;

	//##ModelId=43834ABD0297
	virtual Bound*	getBound();// throw (NullPointerException);

	//##ModelId=43834ABD029C
	virtual void	printInfos();

	void changeBound(Bound* mNewBound);

	double getReferenceValue() const;


	virtual void	setSimulatedValue(double dSimulatedNewValue);
	virtual double	getSimulatedValue() const;




private:
	
	//##ModelId=43834ABD029F
	std::string _sName;
	//##ModelId=43834ABD02AC
	double _dValue;

	double _dSimulatedValue;

	//##ModelId=43872FD3003F
	Bound* _mBound;

	double _dReferenceValue;

};

#endif // !defined(AFX_VARIABLE_H__04D26E63_7F48_4183_A917_EF05AB3C9CCD__INCLUDED_)
