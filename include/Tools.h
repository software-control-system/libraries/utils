#ifndef TOOLS_H
#define TOOLS_H
 
#include <string>
#include <sstream>
#include "Constant.h"
#include <math.h>

//#include "TrigonometricValueOutOfBoundException.h"
//#include "DivideByZeroException.h"


/*enum TANGO_STATES
{
	ON,		//0
	OFF,	//
	CLOSE,
	OPEN,
	INSERT,
	EXTRACT,
	MOVING,
	STANDBY,
	FAULT,
	INIT,
	RUNNING,
	ALARM,
	DISABLE,
	UNKNOW
};
*/
using namespace std;


string	itos(int i);
string	ltos(long l);	
string	dtos(double d);

double	RadiansToDegres(double dRadians);
double	DegresToRadians(double dDegres);

double  BrasSinus(double dL,double dAngle);
//double  InverseBrasSinus(double dL,double dTranslation); //throw (DivideByZeroException,TrigonometricValueOutOfBoundException);
double InverseBrasSinus(double dL,double dTranslation);


double BrasTangente(double dL,double dAngle);
double InverseBrasTangente(double dL,double dTranslation);


std::string GetDeviceState(int iDeviceState);

///Method to make the destruction of a dynamic object
template <class ObjectType>  
void DESTRUCTION(ObjectType*& objectName)
{
	if(objectName)	
	{
		delete objectName;
		objectName = 0;
	}
}

///Return the maximum object of obj1 and obj2
template <class ObjectType>  
ObjectType Maximum(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 <= obj2 ? obj2 : obj1;
}

///Return the minimum object of obj1 and obj2
template <class ObjectType>  
ObjectType Minimum(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 <= obj2 ? obj1 : obj2;
}


///Return true if the obj1 is greater than the obj2
template <class ObjectType>
bool isGreaterThan(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 >= obj2;
}

///Return true if the obj1 is stricktly greater than the obj2
template <class ObjectType>
bool isStrictlyGreaterThan(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 > obj2;
}

///Return true if the obj1 is less than the obj2
template <class ObjectType>
bool isLessThan(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 <= obj2;
}

///Return true if the obj1 is stricktly less than the obj2
template <class ObjectType>
bool isStrictlyLessThan(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 < obj2;
}

///Return true if the obj1 is equal to obj2
template <class ObjectType>
bool areEquals(ObjectType& obj1,ObjectType& obj2)
{
	return obj1 == obj2;
}





#endif

