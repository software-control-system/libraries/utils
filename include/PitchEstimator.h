// PitchEstimator.h: interface for the PitchEstimator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PITCHESTIMATOR_H__74E951F6_45DC_479B_A53E_489542CE866D__INCLUDED_)
#define AFX_PITCHESTIMATOR_H__74E951F6_45DC_479B_A53E_489542CE866D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class PitchEstimator  
{
public:
	PitchEstimator(	double dEnergy1,double dPitch1,
					double dEnergy2,double dPitch2,
					double dEnergy3,double dPitch3);
	virtual ~PitchEstimator();
	void	computePitchCoefficients(	double e1,double p1,
										double e2,double p2,
										double e3,double p3);
	double	getCoefficientA();
	double	getCoefficientB();
	double	getCoefficientC();

private:
	double _dEnergy1;
	double _dEnergy2;
	double _dEnergy3;
	double _dPitch1;
	double _dPitch2;
	double _dPitch3;

	double _dCoeffA;
	double _dCoeffB;
	double _dCoeffC;


};

#endif // !defined(AFX_PITCHESTIMATOR_H__74E951F6_45DC_479B_A53E_489542CE866D__INCLUDED_)
