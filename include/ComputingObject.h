// ComputingObject.h: interface for the ComputingObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPUTINGOBJECT_H__2629E225_9D09_4A63_958D_1095B6CECAF7__INCLUDED_)
#define AFX_COMPUTINGOBJECT_H__2629E225_9D09_4A63_958D_1095B6CECAF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Monochromator.h"

//##ModelId=439D8834020D
class ComputingObject  
{
public:
	//##ModelId=439D88340218
	ComputingObject();
	//##ModelId=439D88340219
	virtual ~ComputingObject();
		
	//virtual double computeValue(double dXValue, double dYValue);
	//##ModelId=439D8834021B
	virtual double  computeValue(double dValue);
	//##ModelId=439D8834021E
	virtual double  computeValue()=0;

	//##ModelId=439D88340220
	virtual void 	printInfos()=0;
};

#endif // !defined(AFX_COMPUTINGOBJECT_H__2629E225_9D09_4A63_958D_1095B6CECAF7__INCLUDED_)
