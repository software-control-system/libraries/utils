#ifndef XSTRING_H
#define XSTRING_H

#include <sstream>
#include <string>

template<class T>   class XString{

public:	
	
	static	T convertFromString(const std::string& s)
	{
		std::istringstream in(s);
		T x;
		if (in >> x)
			return x;
		// some sort of error handling goes here...
		return 0;
		/* Exemple:
		Tango::DevShort attr_gain_write = XString<Tango::DevShort>::convertFromString(mem_value);
		*/
	} 
//
	static	std::string convertToString(const T & t)
	{
		std::ostringstream out ;
		
		if (out << std::fixed << t   )
			return out.str();
		// some sort of error handling goes here...
		return 0;
		/* Exemple:
		string frequence = XString<Tango::DevDouble>::convertToString(attr_frequency_write);
		*/
	} 

};
#endif
