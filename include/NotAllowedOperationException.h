// NotAllowedOperationException.h: interface for the NotAllowedOperationException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NOTALLOWEDOPERATIONEXCEPTION_H__3027BCAD_8BE8_4C3F_9C5C_C9F35F09B4D7__INCLUDED_)
#define AFX_NOTALLOWEDOPERATIONEXCEPTION_H__3027BCAD_8BE8_4C3F_9C5C_C9F35F09B4D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OperationException.h"

///This class is used to manage exception when an operation is not allowed.
class NotAllowedOperationException : public OperationException  
{
public:
  NotAllowedOperationException() throw();
  virtual ~NotAllowedOperationException() throw();

	NotAllowedOperationException(std::string sOperationNotAllowed,std::string sReason,std::string sOrigin,std::string sFileName,unsigned int iLineNumber);

	virtual std::string makeDescription();
	virtual std::string makeReason();
	virtual std::string makeOrigin(std::string sOrigin);

private:
	std::string _sOperationNotAllowed;
	std::string _sReason;
};

#endif // !defined(AFX_NOTALLOWEDOPERATIONEXCEPTION_H__3027BCAD_8BE8_4C3F_9C5C_C9F35F09B4D7__INCLUDED_)
