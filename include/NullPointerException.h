// NullPointerException.h: interface for the NullPointerException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NULLPOINTEREXCEPTION_H__D8CF7767_F5D5_4E8E_BD23_A9190145AC95__INCLUDED_)
#define AFX_NULLPOINTEREXCEPTION_H__D8CF7767_F5D5_4E8E_BD23_A9190145AC95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Exception.h"

///This class is used to manage exception due to a null pointer
//##ModelId=43834ABF00EB
class NullPointerException : public Exception  
{
public:

	//##ModelId=43834ABF00F7
	NullPointerException() throw();
	//##ModelId=43834ABF00F8
	NullPointerException(std::string sElementName) throw();
	//##ModelId=43834ABF00FA
	NullPointerException(std::string sElementName,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw();
	//##ModelId=43834ABF00FF
	virtual ~NullPointerException() throw();

	//##ModelId=43834ABF0106
	virtual std::string makeDescription();
	//##ModelId=43834ABF0108
	virtual std::string makeReason();
	//##ModelId=43834ABF010A
	virtual std::string makeOrigin(std::string sOrigin);

private:
	//##ModelId=43834ABF010E
	std::string _sElementName;
};

#endif // !defined(AFX_NULLPOINTEREXCEPTION_H__D8CF7767_F5D5_4E8E_BD23_A9190145AC95__INCLUDED_)
