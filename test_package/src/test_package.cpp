#include <iostream>

#include <Duration.h>

int main()
{
    Duration d = Duration();
    d.Start();
    for (volatile int i = 0; i < 10000000; i++);
    d.Stop();

    std::cout << d.GetDuration() << '\n';

    return 0;
}
