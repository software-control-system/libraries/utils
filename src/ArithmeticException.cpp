// ArithmeticException.cpp: implementation of the ArithmeticException class
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "ArithmeticException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC30146
ArithmeticException::ArithmeticException() throw()
{

}

///Destructor
//##ModelId=43834AC30147
ArithmeticException::~ArithmeticException() throw()
{

}
