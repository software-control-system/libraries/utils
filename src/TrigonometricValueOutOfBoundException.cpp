// TrigonometricValueOutOfBoundException.cpp: implementation of the TrigonometricValueOutOfBoundException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "TrigonometricValueOutOfBoundException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834ABD0398
TrigonometricValueOutOfBoundException::TrigonometricValueOutOfBoundException() throw()
{

}

///Destructor
//##ModelId=43834ABD03BB
TrigonometricValueOutOfBoundException::~TrigonometricValueOutOfBoundException() throw()
{

}

///This constructor requires 5 parameters :
///		- the name of the element which is not in the trigonometric allowed bound
///		- the bad value which is not compliant with the Bound of the trigonometric function
///		- the minimal bound value of the trigonometric function
///		- the maximal bound value of the trigonometric function 
///		- the name of the trigonometric function
//##ModelId=43834ABD03A6
TrigonometricValueOutOfBoundException::TrigonometricValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax,std::string sFunctionName)  throw() : _sElementName(sElementName),_dValue(dValue),_dValueMin(dValueMin),_dValueMax(dValueMax),_sFunctionName(sFunctionName)
{		
		setType("TrigonometricValueOutOfBoundException");
		setLineNumber(0);
		setFileName("");
 		
		setDescription(makeDescription());
		setReason(makeReason());
		setOrigin("");

}


///This constructor requires 8 parameters :
///		- the name of the element which is not in the trigonometric allowed Bound
///		- the bad value which is not compliant with the bound of the trigonometric function
///		- the minimal bound value of the trigonometric function
///		- the maximal bound value of the trigonometric function 
///		- the name of the trigonometric function
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834ABD03AC
TrigonometricValueOutOfBoundException::TrigonometricValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax,std::string sFunctionName,std::string sOrigin,std::string sFilename,unsigned int iLineNumber) throw() : _sElementName(sElementName),_dValue(dValue),_dValueMin(dValueMin),_dValueMax(dValueMax),_sFunctionName(sFunctionName)
{		
		setType("TrigonometricValueOutOfBoundException");
		setLineNumber(iLineNumber);
		setFileName(sFilename);
		
		setDescription(makeDescription());
		setReason(makeReason());
		setOrigin(makeOrigin(sOrigin));
}


///The message provides by the trigonometric value out of Bound exception
//##ModelId=43834ABD03BD
std::string TrigonometricValueOutOfBoundException::makeDescription()
{ 
	std::string	sExceptionMessage	=	"The value ";
			sExceptionMessage	+=	dtos(_dValue);
			sExceptionMessage	+=  " of the ";
			sExceptionMessage	+=  _sElementName;
			sExceptionMessage	+=	" variable must be in the allowed Bound [" ;
			sExceptionMessage	+=	dtos(_dValueMin);
			sExceptionMessage	+=	","; 
			sExceptionMessage	+=	dtos(_dValueMax);
			sExceptionMessage	+=	"] for the function "; 
			sExceptionMessage	+=	_sFunctionName;
	return sExceptionMessage;
}

///The reason message provides by the trigonometric value out of Bound exception
//##ModelId=43834ABD03BF
std::string TrigonometricValueOutOfBoundException::makeReason()
{
	std::string	sReasonMessage = "DATA_OUT_OF_RANGE --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the trigonometric value out of Bound exception
//##ModelId=43834ABD03C1
std::string TrigonometricValueOutOfBoundException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
