
#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Tools.h"
#include "Xstring.h"

std::string GetDeviceState(int iDeviceState)
{

	switch (iDeviceState)
	{
	case 0:
		return "ON";
		break;
	case 1:
		return "OFF";
		break;
	case 2:
		return "CLOSE";
		break;
	case 3:
		return "OPEN";
		break;
	case 4:
		return "INSERT";
		break;
	case 5:
		return "EXTRACT";
		break;
	case 6:
		return "MOVING";
		break;
	case 7:
		return "STANDBY";
		break;
	case 8:
		return "FAULT";
		break;
	case 9:
		return "INIT";
		break;
	case 10:
		return "RUNNING";
		break;
	case 11:
		return "ALARM";
		break;
	case 12:
		return "DISABLE";
		break;
	case 13:
		return "UNKNOW";
		break;
	default:
		return "NO COMMUNICATION / UNDEFINED";
		break;
	}
}


///Tool method to convert a int to a string
string itos(int i)	
{
	return XString<int>::convertToString(i);
}

///Tool method to convert a long to a string
string ltos(long l)	
{
	return XString<long>::convertToString(l);
}

///Tool method to convert a double to a string
string dtos(double d)	
{
	return XString<double>::convertToString(d);
}

///Method to change an angle from radians to degres
///The equation used is \f[ AngleDegres = \frac{AngleRadians*180.0}{\pi} \f]
double RadiansToDegres(double dRadians) 
{
	return dRadians * 180.0 / MATH_CONSTANT::PI;
}

///Method to change an angle from degres to radians
///The equation used is \f[ AngleRadians = \frac{AngleDegres*\pi}{180.0} \f]
double DegresToRadians(double dDegres) 
{
	return dDegres * MATH_CONSTANT::PI / 180.0;
}

///Method to convert an angle movement to a translation movement\n
///The angle must be in radians !\n
///The equation used is : \f[ x = L*sin(\alpha) \f] with :\n
/// \f$ x \f$ the translation movement value,\n
/// \f$ L \f$ the length of the sinus arm,\n
/// \f$ \alpha \f$ the angle movement value in radians
double BrasSinus(double dL,double dAngle)
{
	return dL*sin(dAngle);
}


///Method to convert a translation movement to a angle movement\n
///The angle computed is given in radians !\n
///The equation used is : \f[ \alpha = arcsin(frac{x}{L}) \f] with :\n
/// \f$ x \f$ the translation movement value,\n
/// \f$ L \f$ the length of the sinus arm,\n
/// \f$ \alpha \f$ the angle movement value in radians
///\exception This method throw a DivideByZeroException exception if the mirror angle is equal to zero
///\exception This method throw a TrigonometricValueOutOfBoundException exception if the value to compute the \f$ \arcsin \f$ is not in the range \f$ [-1;1] \f$ 	
double InverseBrasSinus(double dL,double dTranslation) //// throw (DivideByZeroException,TrigonometricValueOutOfBoundException)
{
//	if (dL == 0.0) throw DivideByZeroException("Inverse Bras Sinus","L","InverseBrasSinus()",__FILE__,__LINE__);
	double dQuotient		= dTranslation/dL;
//	if ((-1.0 > dQuotient) || (dQuotient > 1.0)) throw TrigonometricValueOutOfBoundException("Inverse Bras Sinus",dQuotient,-1.0,1.0,"Arcsin","InverseBrasSinus()",__FILE__,__LINE__);

	return asin(dQuotient);
}




///Method to convert an angle movement to a translation movement\n
///The angle must be in radians !\n
///The equation used is : \f[ x = L*tan(\alpha) \f] with :\n
/// \f$ x \f$ the translation movement value,\n
/// \f$ L \f$ the length of the tangent arm,\n
/// \f$ \alpha \f$ the angle movement value in radians
double BrasTangente(double dL,double dAngle)
{
	return dL*tan(dAngle);
}


///Method to convert a translation movement to a angle movement\n
///The angle computed is given in radians !\n
///The equation used is : \f[ \alpha = arctan(frac{x}{L}) \f] with :\n
/// \f$ x \f$ the translation movement value,\n
/// \f$ L \f$ the length of the tangent arm,\n
/// \f$ \alpha \f$ the angle movement value in radians
///\exception This method throw a DivideByZeroException exception if the mirror angle is equal to zero
///\exception This method throw a TrigonometricValueOutOfBoundException exception if the value to compute the \f$ \arcsin \f$ is not in the range \f$ [-1;1] \f$ 	
double InverseBrasTangente(double dL,double dTranslation)
{
//	if (dL == 0.0) throw DivideByZeroException("Inverse Bras Sinus","L","InverseBrasSinus()",__FILE__,__LINE__);
	
//	double dQuotient		= dTranslation/dL;

	//	if ((-1.0 > dQuotient) || (dQuotient > 1.0)) throw TrigonometricValueOutOfBoundException("Inverse Bras Sinus",dQuotient,-1.0,1.0,"Arcsin","InverseBrasSinus()",__FILE__,__LINE__);

	double dQuotient		= dTranslation/dL;
	return atan(dQuotient);

//	double dQuotient		= dTranslation/dL;
//	return atan2(dTranslation,dL);


}




