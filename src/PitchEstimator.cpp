// PitchEstimator.cpp: implementation of the PitchEstimator class.
//
//////////////////////////////////////////////////////////////////////

#include "PitchEstimator.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


///Solving system :
///\f[ a+b*e_1+c*e_1^2 = p_1 \f]
///\f[ a+b*e_2+c*e_2^2 = p_2 \f]
///\f[ a+b*e_3+c*e_3^2 = p_3 \f]
PitchEstimator::PitchEstimator(	double dEnergy1,double dPitch1,
								double dEnergy2,double dPitch2,
								double dEnergy3,double dPitch3)
{
	computePitchCoefficients(dEnergy1,dPitch1,dEnergy2,dPitch2,dEnergy3,dPitch3);
}

PitchEstimator::~PitchEstimator()
{

}


///Results of the system are :
///\f[a = \frac{-e_2^2*e_1*p_3+p_2*e_1*e_3^2+e_1^2*e_2*p_3-e_1^2*e_3*p_2-p_1*e_2*e_3^2+e_2^2*e_3*p_1}{e_2*e_1^2-e_2*e_3^2+e_1*e_3^2-e_1*e_2^2+e_3*e_2^2-e_3*e_1^2} \f]
///\f[b = \frac{-e_2^2*p_1+e_2^2*p_3+p_1*e_3^2+p_2*e_1^2-p_2*e_3^2-e_1^2*p_3}{e_2*e_1^2-e_2*e_3^2+e_1*e_3^2-e_1*e_2^2+e_3*e_2^2-e_3*e_1^2} \f]
///\f[c = -\frac{-e_2*p_1+e_3*p_1+e_2*p_3-e_1*p_3+e_1*p_2-e_3*p_2}{e_2*e_1^2-e_2*e_3^2+e_1*e_3^2-e_1*e_2^2+e_3*e_2^2-e_3*e_1^2} \f]
void PitchEstimator::computePitchCoefficients(	double e1,double p1,
												double e2,double p2,
												double e3,double p3)
{
	double dDeno = e2*e1*e1-e2*e3*e3+e1*e3*e3-e1*e2*e2+e3*e2*e2-e3*e1*e1;

	_dCoeffA = (-e2*e2*e1*p3+p2*e1*e3*e3+e1*e1*e2*p3-e1*e1*e3*p2-p1*e2*e3*e3+e2*e2*e3*p1)/dDeno;   
	_dCoeffB = (-e2*e2*p1+e2*e2*p3+p1*e3*e3+p2*e1*e1-p2*e3*e3-e1*e1*p3)/dDeno;   
	_dCoeffC = -(-e2*p1+e3*p1+e2*p3-e1*p3+e1*p2-e3*p2)/dDeno; 
}

double PitchEstimator::getCoefficientA()
{
	return _dCoeffA;
}

double PitchEstimator::getCoefficientB()
{
	return _dCoeffB;
}

double PitchEstimator::getCoefficientC()
{
	return _dCoeffC;
}



