// NullPointerException.cpp: implementation of the NullPointerException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "NullPointerException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834ABF00F7
NullPointerException::NullPointerException() throw()
{

}
///Destructor
//##ModelId=43834ABF00FF
NullPointerException::~NullPointerException() throw()
{

}


///This constructor requires 1 parameter :
///		- the name of the null pointer element
//##ModelId=43834ABF00F8
NullPointerException::NullPointerException(std::string sElementName)  throw() : _sElementName(sElementName)
{	
	setType("NullPointerException");
	setLineNumber(0);
	setFileName("");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");

}

///This constructor requires 4 parameters :
///		- the name of the null pointer element
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834ABF00FA
NullPointerException::NullPointerException(std::string sElementName,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw() : _sElementName(sElementName)
{	
	setType("NullPointerException");
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}


///The message provides by the null pointer exception
//##ModelId=43834ABF0106
std::string NullPointerException::makeDescription()
{
	std::string	sExceptionMessage = "The element ";
			sExceptionMessage+= _sElementName;
			sExceptionMessage+= " is a null pointer !!!";
	return sExceptionMessage;
}


///The reason message provides by the null pointer exception
//##ModelId=43834ABF0108
std::string NullPointerException::makeReason()
{
	std::string	sReasonMessage = "OPERATION_NOT_ALLOWED --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the null pointer exception
//##ModelId=43834ABF010A
std::string NullPointerException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
