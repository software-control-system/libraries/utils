// Bound.cpp: implementation of the Bound class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Bound.h"
#include <iostream>
//using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43872EC80066
Bound::Bound()
{

}

///Destructor
//##ModelId=43872EC8006B
Bound::~Bound()
{

}

///This constructor requires 3 parameters : 
///		- the name of the bound (indicate the objet bound)
///		- the lower bound value
///		- the upper bound value
//##ModelId=43872EC80067
Bound::Bound(std::string sName,double dLowerBound,double dUpperBound) : _sName(sName),_dLowerBound(dLowerBound),_dUpperBound(dUpperBound)
{

}

///Change the name of the Bound.	
//##ModelId=43872EC8006D
inline void Bound::setName(std::string sName) 
{
	_sName = sName;
}

///Return the name of the Bound.
//##ModelId=43872EC80076
inline std::string Bound::getName() const 
{
	return _sName;
}

///Set the Lower Bound value.
//##ModelId=43872EC80078
void	Bound::setLowerBound(double dLowerBound)
{
	_dLowerBound = dLowerBound;
}
///Return the Lower Bound value.
//##ModelId=43872EC8007B
double	Bound::getLowerBound() const
{
	return _dLowerBound;
}
///Set the upper Bound value.
//##ModelId=43872EC8007D
void	Bound::setUpperBound(double dUpperBound)
{
	_dUpperBound = dUpperBound;
}
///Return the upper Bound value.
//##ModelId=43872EC80085
double  Bound::getUpperBound() const
{
	return _dUpperBound;
}

///Update the Bound with the mBound Bound object values.
//##ModelId=43872EC80087
void Bound::setBound(Bound* mBound)
{
	setName(mBound->getName());
	setLowerBound(mBound->getLowerBound());
	setUpperBound(mBound->getUpperBound());
}

///Update the Bound members values according the three parameters (name,min,max)
//##ModelId=43872EC80089
void Bound::setBound(std::string sName,double dLowerBound,double dUpperBound)
{
	setName(sName);
	setLowerBound(dLowerBound);
	setUpperBound(dUpperBound);
}

///Copy constructor
//##ModelId=43872EC8008D
Bound::Bound(const Bound& mBound)
{
	setBound(mBound.getName(),mBound.getLowerBound(),mBound.getUpperBound());
}

///Operator=
//##ModelId=439D88340285
Bound& Bound::operator=(Bound* pBound)
{
	
	setBound(pBound);
    return *this;
} 

///Method to check if a value is in the bounds.
///The limits are included : [lowerBound,upperBound]
//##ModelId=439D88340256
bool Bound::isInBound(double dValue) 
{
	return ((getLowerBound() <= dValue) && (dValue  <= getUpperBound()));
}

///Method to check if a value is in the bounds.
///The limits are not included : ]lowerBound,upperBound[
//##ModelId=439D88340276
bool Bound::isStricklyInBound(double dValue) 
{
	return ((getLowerBound() < dValue) && (dValue  < getUpperBound()));
}

void Bound::printInfos()
{
	//YAT_VERBOSE_STREAM("\n##### BOUND " << getName() << " --> [" << getLowerBound() << "," << getUpperBound() << "]" <<  std::endl);
}

