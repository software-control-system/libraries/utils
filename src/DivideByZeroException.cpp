// DivideByZeroException.cpp: implementation of the DivideByZeroException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "DivideByZeroException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC102BC
DivideByZeroException::DivideByZeroException() throw()
{

}

///Destructor
//##ModelId=43834AC102DD
DivideByZeroException::~DivideByZeroException() throw()
{

}

///This constructor requires 2 parameters :
///		- the attribute which the value cannot be computed due to the divide by zero error
///		- the attribute which is null and has done the error
//##ModelId=43834AC102BD
DivideByZeroException::DivideByZeroException(std::string sExpressionToCompute,std::string sZeroValue)  throw() : _sExpressionToCompute(sExpressionToCompute),_sZeroValue(sZeroValue)
{	
	setLineNumber(0);
	setFileName("");
	setType("DivideByZeroException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");

}

///This constructor requires 5 parameters :
///		- the attribute which the value cannot be computed due to the divide by zero error
///		- the attribute which is null and has done the error
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834AC102CC
DivideByZeroException::DivideByZeroException(std::string sExpressionToCompute,std::string sZeroValue,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw() : _sExpressionToCompute(sExpressionToCompute),_sZeroValue(sZeroValue)
{	
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	setType("DivideByZeroException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}

///The description message provides by the divide by zero exception
//##ModelId=43834AC102EA
std::string DivideByZeroException::makeDescription()
{
	std::string	sExceptionMessage = "Computing of the ";
			sExceptionMessage+= _sExpressionToCompute;
			sExceptionMessage+= " not possible due to ";
			sExceptionMessage+= _sZeroValue;
			sExceptionMessage+= " equal to zero";

	return sExceptionMessage;
}

///The reason message provides by the divide by zero exception
//##ModelId=43834AC102EC
std::string DivideByZeroException::makeReason()
{
	std::string	sReasonMessage = "OPERATION_NOT_ALLOWED --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}


///The origin message provides by the divide by zero exception
//##ModelId=43834AC102FA
std::string DivideByZeroException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
