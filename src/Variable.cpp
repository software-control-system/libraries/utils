// Variable.cpp: implementation of the Variable class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Variable.h"
#include <iostream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834ABD027F
Variable::Variable()
{

}

///This constructor requires 3 parameters :
///		- the name of the variable
///		- the lower variable bound value
///		- the upper variable bound value
//##ModelId=43834ABD0280
Variable::Variable(std::string sName,double dLowerVariableBound,double dUpperVariableBound,double dReferenceValue) : 
_sName(sName),
_dValue(-1.0),
_dSimulatedValue(-1.0),
_dReferenceValue(dReferenceValue)
{
	_mBound = new Bound(sName,dLowerVariableBound,dUpperVariableBound);
}

///Destructor
//##ModelId=43834ABD0285
Variable::~Variable()
{
	if (_mBound) 
	{
		delete _mBound;
		_mBound = 0;
	}
}

///Change the value of the variable.
///\exception This method throw a ValueOutOfBoundException exception if the variable value is not in the allowed bound.
//##ModelId=43834ABD028D
void Variable::setValue(double dNewValue) //// throw (ValueOutOfBoundException)
{

	if ( getBound()->isInBound(dNewValue)) 
	{
		_dValue = dNewValue;
	}
	else throw ValueOutOfBoundException(getName(),dNewValue,_mBound->getLowerBound(),_mBound->getUpperBound(),"Variable::setValue(double dNewValue)",__FILE__,__LINE__);	

}

///Change the value of the variable without checking the boundaries conditions
void Variable::setUncheckValue(double dNewValue)
{
		_dValue = dNewValue;
}

///Return the value of the variable.
//##ModelId=43834ABD0290
double	Variable::getValue() const 
{
	return _dValue;
}


double Variable::getReferenceValue() const 
{
	return _dReferenceValue;
}




///Change the Simulated value of the variable.
void Variable::setSimulatedValue(double dSimulatedNewValue)
{
	_dSimulatedValue = dSimulatedNewValue;
}


///Return the Simulated value of the variable.
double	Variable::getSimulatedValue() const 
{
	return _dSimulatedValue;
}










///Set the name of the variable.
//##ModelId=43834ABD0292
void	Variable::setName(std::string sNewName) 
{
	_sName = sNewName;
}

///Return the name of the variable.
//##ModelId=43834ABD0295
std::string	Variable::getName() const 
{
	return _sName;
}

///Return a reference to the variable bound
///\exception This method throw a NullPointerException exception if the bound is null.
//##ModelId=43834ABD0297
Bound* Variable::getBound() //// throw (NullPointerException)
{
	if (_mBound==0) throw NullPointerException("_mBound","Variable::getBound()",__FILE__,__LINE__);
	return _mBound;
}


///Print infos about Variable object\n
///Used mainly in DEBUG mode.
//##ModelId=43834ABD029C
void Variable::printInfos() 
{
	//YAT_VERBOSE_STREAM("\n##### VARIABLE " << endl);
	//YAT_VERBOSE_STREAM("\tVariable --> " << getName() << " : " << getValue() << std::endl);
	_mBound->printInfos(); 
}

///Change the bound value of the variable with the mNewBound values
void Variable::changeBound(Bound* mNewBound)
{
	_mBound->setBound(mNewBound);
}

