// FileNotFoundException.cpp: implementation of the FileNotFoundException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "FileNotFoundException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC101C3
FileNotFoundException::FileNotFoundException() throw()
{

}

///Destructor
//##ModelId=43834AC101D6
FileNotFoundException::~FileNotFoundException() throw()
{

}

///This constructor requires 4 parameters :
///		- the name of the file which cannot be opened
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown
//##ModelId=43834AC101D1
FileNotFoundException::FileNotFoundException(std::string sNameOfTheFile,std::string sOrigin,std::string sFileName,unsigned int iLineNumber)  throw()
{
	
	setNameOfTheFile(sNameOfTheFile);

	setLineNumber(iLineNumber);
	setFileName(sFileName);
	setType("FileNotFoundException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}



///The description message provides by the file not found exception
//##ModelId=43834AC101D8
std::string FileNotFoundException::makeDescription()
{
	std::string	sExceptionMessage = "Can't open or read ";
			sExceptionMessage+= "'";
			sExceptionMessage+= getNameOfTheFile();
			sExceptionMessage+= "'";
			sExceptionMessage+= " file !!! ";

	return sExceptionMessage;
}

///The reason message provides by the file not found exception
//##ModelId=43834AC101DA
std::string FileNotFoundException::makeReason()
{
	std::string	sReasonMessage = "FILE_READ_ERROR --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}


///The origin message provides by the file not found exception
//##ModelId=43834AC101DC
std::string FileNotFoundException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
