// NotAllowedOperationException.cpp: implementation of the NotAllowedOperationException class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32 
#pragma warning(disable:4786)
#endif

#include "NotAllowedOperationException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
NotAllowedOperationException::NotAllowedOperationException() throw()
{

}
///Destructor
NotAllowedOperationException::~NotAllowedOperationException() throw()
{

}

///This constructor requires 5 parameters :
///		- the operation which is not allowed
///		- the reason why it is not allowed
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
NotAllowedOperationException::NotAllowedOperationException(std::string sOperationNotAllowed,std::string sReason,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) : _sOperationNotAllowed(sOperationNotAllowed),_sReason(sReason)
{	
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	setType("NotAllowedOperationException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}

///The description message provides by the not allowed operation exception
std::string NotAllowedOperationException::makeDescription()
{
	std::string	sExceptionMessage = "The operation ";
			sExceptionMessage+= _sOperationNotAllowed;
			sExceptionMessage+= " is not allowed and the reason is ";
			sExceptionMessage+= _sReason;

	return sExceptionMessage;
}

///The reason message provides by the not allowed operation exception
std::string NotAllowedOperationException::makeReason()
{
	std::string	sReasonMessage = "OPERATION_NOT_ALLOWED --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}


///The origin message provides by the not allowed operation exception
std::string NotAllowedOperationException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}

