// ValueOutOfBound.cpp: implementation of the ValueOutOfBound class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif

#include "ValueOutOfBoundException.h"
//#include "Tools.cpp"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834ABD02CD
ValueOutOfBoundException::ValueOutOfBoundException() throw()
{

}

///Destructor
//##ModelId=43834ABD02E5
ValueOutOfBoundException::~ValueOutOfBoundException() throw()
{

}

///This constructor requires 4 parameters :
///		- the name of the element which is not in its allowed bound
///		- the value which is out of the bound
///		- the minimal bound value
///		- the maximal bound value
//##ModelId=43834ABD02CE
ValueOutOfBoundException::ValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax)  throw(): _sElementName(sElementName),_dValue(dValue),_dValueMin(dValueMin),_dValueMax(dValueMax)
{	
	setLineNumber(0);
	setFileName("");
	setType("ValueOutOfBoundException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");
	

}

///This constructor requires 3 parameters :
///		- the name of the element which is not in its allowed bound
///		- the value which is out of the bound
///		- the bound object which contains the boundary conditions
//##ModelId=438739560186
ValueOutOfBoundException::ValueOutOfBoundException(std::string sElementName,double dValue,Bound* mBound) throw() : _sElementName(sElementName),_dValue(dValue),_dValueMin(mBound->getLowerBound()),_dValueMax(mBound->getUpperBound())
{	
	setLineNumber(0);
	setFileName("");
	setType("ValueOutOfBoundException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");
}

///This constructor requires 7 parameters :
///		- the name of the element which is not in its allowed bound
///		- the value which is out of the bound
///		- the minimal bound value
///		- the maximal bound value
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834ABD02DD
ValueOutOfBoundException::ValueOutOfBoundException(std::string sElementName,double dValue,double dValueMin, double dValueMax,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw() : _sElementName(sElementName),_dValue(dValue),_dValueMin(dValueMin),_dValueMax(dValueMax)
{	
	setType("ValueOutOfBoundException");
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}

///The message provides by the value out of range exception
//##ModelId=43834ABD02EA
std::string ValueOutOfBoundException::makeDescription()
{ 
	std::string	sExceptionMessage	=	"The value ";
			sExceptionMessage	+=	dtos(_dValue);
			sExceptionMessage	+=  " of the ";
			sExceptionMessage	+=  _sElementName;
			sExceptionMessage	+=	" variable must be in the allowed bound [" ;
			sExceptionMessage	+=	dtos(_dValueMin);
			sExceptionMessage	+=	","; 
			sExceptionMessage	+=	dtos(_dValueMax);
			sExceptionMessage	+=	"]"; 
	
	return sExceptionMessage;
}

///The reason message provides by the value out of bound exception
//##ModelId=43834ABD02EC
std::string ValueOutOfBoundException::makeReason()
{
	std::string	sReasonMessage = "DATA_OUT_OF_RANGE --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the value out of bound exception
//##ModelId=43834ABD02EE
std::string ValueOutOfBoundException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
