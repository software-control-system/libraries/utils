// ParseException.cpp: implementation of the ParseException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "ParseException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834ABF00BA
ParseException::ParseException() throw()
{

}
///Destructor
//##ModelId=43834ABF00C8
ParseException::~ParseException() throw()
{

}


///This constructor requires 1 parameter :
///		- the name of the null pointer element
//##ModelId=43834ABF00BB
ParseException::ParseException(std::string sElementName)  throw() : _sElementName(sElementName)
{	
	setType("NullPointerException");
	setLineNumber(0);
	setFileName("");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");

}

///This constructor requires 4 parameters :
///		- the name of the file or the buffer which can't be parsed
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834ABF00BD
ParseException::ParseException(std::string sElementName,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw() : _sElementName(sElementName)
{	
	setType("ParseException");
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}


///The message provides by the parse exception
//##ModelId=43834ABF00CA
std::string ParseException::makeDescription()
{
	std::string	sExceptionMessage = "The parsing of ";
			sExceptionMessage+= _sElementName;
			sExceptionMessage+= " encountered an problem !!!";
	return sExceptionMessage;
}


///The reason message provides by the parse exception
//##ModelId=43834ABF00CC
std::string ParseException::makeReason()
{
	std::string	sReasonMessage = "FILE_READ_ERROR --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the parse exception
//##ModelId=43834ABF00CE
std::string ParseException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
