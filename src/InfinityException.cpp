// InfinityException.cpp: implementation of the InfinityException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "InfinityException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
InfinityException::InfinityException() throw()
{

}

///Destructor
InfinityException::~InfinityException() throw()
{

}

///This constructor requires 2 parameters :
///		- the expression computed which give an infinity value
///		- the attribute which is infinity and has done the error
InfinityException::InfinityException(std::string sExpressionToCompute,std::string sInfinityValue)  throw() : 
_sExpressionToCompute(sExpressionToCompute),_sInfinityValue(sInfinityValue)
{	
	setLineNumber(0);
	setFileName("");
	setType("InfinityException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");

}

///This constructor requires 5 parameters :
///		- the expression computed which give an infinity value
///		- the attribute which is infinity and has done the error
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
InfinityException::InfinityException(std::string sExpressionToCompute,
									 std::string sInfinityValue,
									 std::string sOrigin,
									 std::string sFileName,
									 unsigned int iLineNumber) throw() : 
_sExpressionToCompute(sExpressionToCompute),_sInfinityValue(sInfinityValue)
{	
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	setType("InfinityException");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}

///The description message provides by the infinity exception
std::string InfinityException::makeDescription()
{
	std::string	sExceptionMessage = "Computing of the ";
				sExceptionMessage+= _sExpressionToCompute;
				sExceptionMessage+= " make ";
				sExceptionMessage+= _sInfinityValue;
				sExceptionMessage+= " equal to infinity";

	return sExceptionMessage;
}

///The reason message provides by the infinity exception
std::string InfinityException::makeReason()
{
	std::string	sReasonMessage = "OPERATION_NOT_ALLOWED --> ";
				sReasonMessage+= getType();
			
	return sReasonMessage;
}


///The origin message provides by the infinity exception
std::string InfinityException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
				sOriginMessage+= " (l:";
				sOriginMessage+= ltos(getLineNumber());
				sOriginMessage+= ") ";
				sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
