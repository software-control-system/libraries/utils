// Exception.cpp: implementation of the Exception class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Exception.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC10221
Exception::Exception() throw()
{

}

///Destructor
//##ModelId=43834AC10222
Exception::~Exception() throw()
{

}

///Return the reason string of the exception.
//##ModelId=43834AC10236
std::string Exception::getReason()
{
	return _sReason;
}

///Return the description string of the exception.
//##ModelId=43834AC10238
string Exception::getDescription()
{
	return _sDescription;
}

///Return the origin string of the exception.
//##ModelId=43834AC1023E
string Exception::getOrigin()
{
	return _sOrigin;
}

///Return the line number which throw the exception.
//##ModelId=43834AC10240
unsigned int Exception::getLineNumber() 
{
	return _iLineNumber;
}

///Return the name of the file which throw the exception.
//##ModelId=43834AC10242
std::string Exception::getFileName()
{
	return _sFileName;
}

///Return the type of the exception thrown.
//##ModelId=43834AC10244
std::string Exception::getType()
{
	return _sType;
}

///Set the reason string of the exception.
//##ModelId=43834AC10246
void Exception::setReason(std::string sReason)
{
	_sReason = sReason;
}

///Set the description string of the exception.
//##ModelId=43834AC10249
void Exception::setDescription(std::string sDescription)
{
	_sDescription = sDescription;
}

///Set the origin string of the exception.
//##ModelId=43834AC1024F
void Exception::setOrigin(std::string sOrigin)
{
	_sOrigin = sOrigin;
}

///Set the line number which throw the exception.
//##ModelId=43834AC10252
void Exception::setLineNumber(unsigned int iLineNumber)
{
	_iLineNumber = iLineNumber;
}

///Set the name of the file which throw the exception.
//##ModelId=43834AC10255
void Exception::setFileName(std::string sFileName)
{
	_sFileName = sFileName;
}

///Set the type of the exception thrown.
//##ModelId=43834AC10258
void Exception::setType(std::string sType)
{
	_sType = sType;
}



