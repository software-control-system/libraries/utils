// NoSuchElementException.cpp: implementation of the NoSuchElementException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "NoSuchElementException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834ABF0117
NoSuchElementException::NoSuchElementException() throw()
{

}

///This constructor requires 1 parameter :
///		- the name of the not found element
//##ModelId=43834ABF0125
NoSuchElementException::NoSuchElementException(std::string sElementName)  throw() : _sElementName(sElementName)
{	
	setType("NoSuchElementException");
	setLineNumber(0);
	setFileName("");
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin("");

}

///This constructor requires 4 parameters :
///		- the name of the not found element
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834ABF0127
NoSuchElementException::NoSuchElementException(std::string sElementName,std::string sOrigin,std::string sFileName,unsigned int iLineNumber)  throw() : _sElementName(sElementName)
{	
	setType("NoSuchElementException");
	setLineNumber(iLineNumber);
	setFileName(sFileName);
	
	setDescription(makeDescription());
	setReason(makeReason());
	setOrigin(makeOrigin(sOrigin));
}



///Destructor
//##ModelId=43834ABF012C
NoSuchElementException::~NoSuchElementException() throw()
{

}

///The message provides by the no such element exception
//##ModelId=43834ABF012E
std::string NoSuchElementException::makeDescription()
{
	std::string	sExceptionMessage = "The wanted element ";
			sExceptionMessage+= _sElementName;
			sExceptionMessage+= " doesn't exist !!!";
	return sExceptionMessage;
}


///The reason message provides by the no such element exception
//##ModelId=43834ABF0130
std::string NoSuchElementException::makeReason()
{
	std::string	sReasonMessage = "OPERATION_NOT_ALLOWED --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the no such element exception
//##ModelId=43834ABF0136
std::string NoSuchElementException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
