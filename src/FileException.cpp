// FileException.cpp: implementation of the FileException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "FileException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC101F1
FileException::FileException() throw()
{

}

///Destructor
//##ModelId=43834AC101F2
FileException::~FileException() throw()
{

}

//##ModelId=43834AC101FB
std::string	FileException::getNameOfTheFile()
{
	return _sNameOfTheFile;
}

//##ModelId=43834AC10200
void	FileException::setNameOfTheFile(std::string sNameOfTheFile)
{
	_sNameOfTheFile = sNameOfTheFile;
}
