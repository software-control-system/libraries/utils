// IndexOutOfBoundException.cpp: implementation of the IndexOutOfBoundException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "IndexOutOfBoundException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC0029D
IndexOutOfBoundException::IndexOutOfBoundException() throw()
{

}

///Destructor
//##ModelId=43834AC002B1
IndexOutOfBoundException::~IndexOutOfBoundException() throw()
{

}

///This constructor requires 4 parameters :
///		- the name of the element which is not in its allowed Bound index
///		- the bad index entered
///		- the Lower Bound index value
///		- the maximal Bound index value
//##ModelId=43834AC0029E
IndexOutOfBoundException::IndexOutOfBoundException(std::string sElementName,long iIndex,long iIndexMin,long iIndexMax) throw() : _sElementName(sElementName),_iIndex(iIndex),_iIndexMin(iIndexMin),_iIndexMax(iIndexMax)
{		
		setLineNumber(0);
		setFileName("");
		setType("IndexOutOfBoundException");

		setOrigin("");	
		setDescription(makeDescription());
		setReason(makeReason());

}


///This constructor requires 7 parameters :
///		- the name of the element which is not in its allowed Bound index
///		- the bad index entered
///		- the Lower Bound index value
///		- the maximal Bound index value
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
//##ModelId=43834AC002A3
IndexOutOfBoundException::IndexOutOfBoundException(std::string sElementName,long iIndex,long iIndexMin,long iIndexMax,std::string sOrigin,std::string sFileName,unsigned int iLineNumber) throw() : _sElementName(sElementName),_iIndex(iIndex),_iIndexMin(iIndexMin),_iIndexMax(iIndexMax)
{		
		setLineNumber(iLineNumber);
		setFileName(sFileName);
		setType("IndexOutOfBoundException");

		setDescription(makeDescription());
		setReason(makeReason());
		setOrigin(makeOrigin(sOrigin));
}


///The message provides by the index out of Bound exception
//##ModelId=43834AC002B3
std::string IndexOutOfBoundException::makeDescription()
{ 
	std::string	sExceptionMessage	=	"The index entered ";
			sExceptionMessage	+=	ltos(_iIndex);
			sExceptionMessage	+=  " of the ";
			sExceptionMessage	+=  _sElementName;
			sExceptionMessage	+=	" variable must be in the allowed bound [" ;
			sExceptionMessage	+=	ltos(_iIndexMin);
			sExceptionMessage	+=	","; 
			sExceptionMessage	+=	ltos(_iIndexMax);
			sExceptionMessage	+=	"["; 
	
	return sExceptionMessage;
}

///The reason message provides by the index out of Bound exception
//##ModelId=43834AC002B5
std::string IndexOutOfBoundException::makeReason()
{
	std::string	sReasonMessage = "DATA_OUT_OF_RANGE --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the index out of Bound exception
//##ModelId=43834AC002BB
std::string IndexOutOfBoundException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
