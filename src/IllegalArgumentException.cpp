// IllegalArgumentException.cpp: implementation of the IllegalArgumentException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "IllegalArgumentException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
//##ModelId=43834AC002DB
IllegalArgumentException::IllegalArgumentException() throw()
{

}

///Destructor
IllegalArgumentException::~IllegalArgumentException() throw()
{

}

/*///This constructor requires 4 parameters :
///		- the name of the element which is an illegal argument
///		- the bad index entered
///		- the number of rights arguments
///		- the list of the rights arguments
IllegalArgumentException::IllegalArgumentException(std::string sElementName,long iIndex,long iIndexMin,long iIndexMax) throw() : _sElementName(sElementName),_iIndex(iIndex),_iIndexMin(iIndexMin),_iIndexMax(iIndexMax)
{		
		setLineNumber(0);
		setFileName("");
		setType("IllegalArgumentException");

		setOrigin("");	
		setDescription(makeDescription());
		setReason(makeReason());

}
*/

///This constructor requires 7 parameters :
///		- the name of the element which is an illegal argument
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown
///		- the bad argument entered
///		- the number of rights arguments
///		- the list of the rights arguments
IllegalArgumentException::IllegalArgumentException(std::string sElementName,
												   std::string sOrigin,
												   std::string sFileName,
												   unsigned int iLineNumber,
												   double dIndex,
												   long iNumberOfArguments,
												   ...)  : 
_sElementName(sElementName),_dIndex(dIndex),_iNbArguments(iNumberOfArguments)
{		

		va_list arg_list;
		va_start (arg_list,iNumberOfArguments);
		
		for (int i=0;i<=_iNbArguments;i++) 
		{	
			double dVal = va_arg(arg_list,double);
			_vArgumentsList.push_back(dVal);
		}
		va_end (arg_list);

		setLineNumber(iLineNumber);
		setFileName(sFileName);
		setType("IllegalArgumentException");

		setDescription(makeDescription());
		setReason(makeReason());
		setOrigin(makeOrigin(sOrigin));
}


///The message provides by the index out of Bound exception
std::string IllegalArgumentException::makeDescription()
{ 
	std::string	sExceptionMessage	=	"The value entered ";
				sExceptionMessage	+=	dtos(_dIndex);
				sExceptionMessage	+=  " of the ";
				sExceptionMessage	+=  _sElementName;
				sExceptionMessage	+=	" variable must be one of the following values [" ;
			
				for (int i=0;i<_iNbArguments-1;i++) 
				{	
					sExceptionMessage	+= dtos(_vArgumentsList[i]);
					sExceptionMessage	+= ";";
				}
				
				sExceptionMessage	+= dtos(_vArgumentsList[_iNbArguments-1]);
				sExceptionMessage	+=	"]";
	
	return sExceptionMessage;
}

///The reason message provides by the index out of Bound exception
std::string IllegalArgumentException::makeReason()
{
	std::string	sReasonMessage = "ILLEGAL_ARGUMENT --> ";
				sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the index out of Bound exception
std::string IllegalArgumentException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
				sOriginMessage+= " (l:";
				sOriginMessage+= ltos(getLineNumber());
				sOriginMessage+= ") ";
				sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}

