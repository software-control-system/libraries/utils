// SquareRootException.cpp: implementation of the SquareRootException class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "SquareRootException.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
SquareRootException::SquareRootException() throw()
{

}

///Destructor
SquareRootException::~SquareRootException() throw()
{

}

///This constructor requires 2 parameters :
///		- the name of the element which is not compliant with the use of the root square function
///		- the bad value which is not compliant with the use of the root square function
SquareRootException::SquareRootException(std::string sElementName,double dValue)  throw() : _sElementName(sElementName),_dValue(dValue)
{		
		setType("SquareRootException");
		setLineNumber(0);
		setFileName("");
 		
		setDescription(makeDescription());
		setReason(makeReason());
		setOrigin("");

}


///This constructor requires 5 parameters :
///		- the name of the element which is not compliant with the use of the root square function
///		- the bad value which is not compliant with the use of the root square function
///		- the method which throw the exception
///		- the name of the file where the exception was thrown
///		- the line number in the file where the exception was thrown.
SquareRootException::SquareRootException(std::string sElementName,double dValue,std::string sOrigin,std::string sFilename,unsigned int iLineNumber) throw() : _sElementName(sElementName),_dValue(dValue)
{		
		setType("SquareRootException");
		setLineNumber(iLineNumber);
		setFileName(sFilename);
		
		setDescription(makeDescription());
		setReason(makeReason());
		setOrigin(makeOrigin(sOrigin));
}


///The message provides by the square root exception
std::string SquareRootException::makeDescription()
{ 
	std::string	sExceptionMessage	=	"The value ";
			sExceptionMessage	+=	dtos(_dValue);
			sExceptionMessage	+=  " of the ";
			sExceptionMessage	+=  _sElementName;
			sExceptionMessage	+=	" variable must be positive";
	return sExceptionMessage;
}

///The reason message provides by the square root exception
std::string SquareRootException::makeReason()
{
	std::string	sReasonMessage = "DATA_OUT_OF_RANGE --> ";
			sReasonMessage+= getType();
			
	return sReasonMessage;
}

///The origin message provides by the square root exception
std::string SquareRootException::makeOrigin(std::string sOrigin)
{
	std::string	sOriginMessage = getFileName();
			sOriginMessage+= " (l:";
			sOriginMessage+= ltos(getLineNumber());
			sOriginMessage+= ") ";
			sOriginMessage+= sOrigin;
			
	return sOriginMessage;
}
